<?php 

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteToTables extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('events', function(Blueprint $table) {
      $table->softDeletes();
    });
    Schema::table('news', function(Blueprint $table) {
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('events', function(Blueprint $table) {
      $table->dropColumn('deleted_at');
    });
    Schema::table('news', function(Blueprint $table) {
      $table->dropColumn('deleted_at');
    });
  }
}