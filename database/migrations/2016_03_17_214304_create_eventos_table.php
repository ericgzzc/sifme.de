<?php 

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('events', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users');
      $table->string('title');
      $table->text('content');
      $table->string('imagen');
      $table->string('address');
      $table->integer('lat');
      $table->integer('long');
      $table->timestamps();
      $table->timestamp('deleted_at');
      $table->timestamp('start_date');
      $table->timestamp('end_date');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('events');
  }
}