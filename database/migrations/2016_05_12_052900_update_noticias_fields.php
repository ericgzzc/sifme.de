<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNoticiasFields extends Migration
{
    public function up()
    {
        Schema::table('news',function(Blueprint $table){
           $table->string('sector')->default('N\A'); 
        });
    }
    public function down()
    {
        Schema::table('news',function(Blueprint $table){
           $table->dropColumn('sector'); 
        });
    }
}
