<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNewsFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_files',function(Blueprint $table){
            $table->dropForeign('news_files_event_id_foreign');
            $table->dropColumn('event_id');
            $table->integer('news_id')->unsigned();
            $table->foreign('news_id')->references('id')->on('news');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_files',function(Blueprint $table){
            $table->dropForeign('news_files_news_id_foreign');
            $table->dropColumn('news_id');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
        });
    }
}
