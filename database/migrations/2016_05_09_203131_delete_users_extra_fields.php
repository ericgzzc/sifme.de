<?php 

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteUsersExtraFields extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('users', function($table) {
      $table->dropColumn('master');
      $table->dropColumn('admin');
      $table->dropColumn('client');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('users', function($table) {
      $table->boolean('master');
      $table->boolean('admin');
      $table->boolean('client');
    });
  }
}