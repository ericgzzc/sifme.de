<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('general_info');
            $table->string('mission');
            $table->string('vision');
            $table->string('bank_name');
            $table->string('bank_account');
            $table->string('clabe');
            $table->string('cardholders_name');
            $table->string('company_name');
            $table->string('contact_email');
            $table->string('contact_phone');
            $table->string('street');
            $table->string('number');
            $table->string('city');
            $table->string('state');
            $table->string('country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('information');
    }
}
