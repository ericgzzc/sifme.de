<?php 

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsFilesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('news_files', function($table) {
      $table->increments('id');
      $table->integer('event_id')->unsigned();
      $table->string('url');
      $table->foreign('event_id')->references('id')->on('events');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('news_files');
  }
}