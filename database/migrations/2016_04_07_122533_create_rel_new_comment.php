<?php 

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelNewComment extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('rel_new_comment', function($table) {
      $table->increments('id');
      $table->integer('new_id')->unsigned();
      $table->integer('comment_id')->unsigned();
      $table->integer('user_id')->unsigned();
      $table->foreign('new_id')->references('id')->on('news');
      $table->foreign('comment_id')->references('id')->on('comments');
      $table->foreign('user_id')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('rel_new_comment');
  }
}