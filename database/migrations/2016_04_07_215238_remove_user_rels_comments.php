<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUserRelsComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('rel_event_comments',function($table){
          $table->dropForeign('rel_event_comments_user_id_foreign');
          $table->dropColumn('user_id');
        });
        Schema::table('rel_new_comments',function($table){
          $table->dropForeign('rel_new_comments_user_id_foreign');
          $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rel_event_comments',function($table){
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('rel_new_comments',function($table){
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
        });
    }
}
