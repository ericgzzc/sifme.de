<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTimestampsFromRelsComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rel_event_comments',function($table){
          $table->dropTimestamps();
        });
        Schema::table('rel_new_comments',function($table){
          $table->dropTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rel_event_comments',function($table){
          $table->timestamps();
        });
        Schema::table('rel_new_comments',function($table){
          $table->timestamps();
        });
    }
}
