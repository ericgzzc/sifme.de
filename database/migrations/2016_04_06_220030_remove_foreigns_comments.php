<?php 

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveForeignsComments extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('comments', function($table) {
      //$table->dropForeign('comments_news_id_foreign');
      $table->dropForeign('comments_news_id_foreign');
      $table->dropForeign('comments_event_id_foreign');
      $table->dropForeign('comments_user_id_foreign');
      $table->dropColumn('news_id');
      $table->dropColumn('event_id');
      $table->dropColumn('user_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('comments', function($table) {
      $table->integer('news_id')->unsigned();
      $table->foreign('news_id')->references('id')->on('news');
      $table->integer('event_id')->unsigned();
      $table->foreign('event_id')->references('id')->on('events');
      $table->integer('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users');
    });
  }
}