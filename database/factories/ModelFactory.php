<?php
use SIFMEDE\User;
use SIFMEDE\Evento;
use SIFMEDE\Noticia;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(SIFMEDE\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10)
    ];
});

$factory->defineAs(SIFMEDE\User::class,'admin', function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10)
    ];
});

$factory->defineAs(SIFMEDE\User::class, 'master' ,function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10)
    ];
});


$factory->define(SIFMEDE\Noticia::class,function(Faker\Generator $faker){
  
  $users = User::all()->lists('id')->all();
  $dt = $faker->dateTimeThisYear;
  //$this->info('Creating sample users...');
  return [
    'user_id' =>  $faker->randomElement($users),
    'title' => $faker->title,
    'content' => $faker->paragraph(),
    'created_at' => $dt,
    'updated_at' => $dt
  ];
});

$factory->defineAs(SIFMEDE\Noticia::class,'deleted', function(Faker\Generator $faker){
  
  $users = User::all()->lists('id')->all();
  $dt = $faker->dateTimeAD;
  
  return [
    'user_id' => $faker->randomElement($users),
    'title' =>$faker->title,
    'content' => $faker->paragraph(),
    'created_at' => $dt,
    'updated_at' => $dt,
    'deleted_at' => $dt
  ];
});

$factory->define(SIFMEDE\Evento::class,function($faker){
  $users = User::all()->lists('id')->all();
  $dt = $faker->dateTimeThisYear;
  return [
    'user_id' => $faker->randomElement($users),
    'title' => $faker->title,
    'content' => $faker->paragraph(),
    'created_at' => $dt,
    'updated_at' => $dt
  ];
});

$factory->define(SIFMEDE\Comment::class,function($faker){
  $users = User::all()->lists('id')->all();
  return [
    'user_id' => $faker->randomElement($users),
    'comment' => $faker->paragraph(),
    'created_at'=> $faker->dateTimeThisMonth()
  ];
});

$factory->define(SIFMEDE\RelEventComment::class,function($faker){
  $events = Evento::all()->lists('id')->all();
  $comments = Comment::all()->lists('id')->all();
  return [
    'event_id' => $faker->randomElement($users),
    'comment_id' => $faker->randomElement($users)
  ];
});

$factory->defineAs(SIFMEDE\RelEventComment::class,'virgin',function($faker){
  return [
    'event_id' => '',
    'comment_id' => ''
  ];
});

$factory->defineAs(SIFMEDE\RelEventComment::class,'withEvent',function($faker){
  $events = Evento::all()->lists('id')->all();
  return [
    'event_id' => $faker->randomElement($events),
    'comment_id' => ''
  ];
});


$factory->define(SIFMEDE\RelNewComment::class,function($faker){
  $news = Noticia::all()->lists('id')->all();
  $comments = Comment::all()->lists('id')->all();
  
});

$factory->defineAs(SIFMEDE\RelNewComment::class,'virgin',function($faker){
  return [
    'new_id' => '',
    'comment_id' => ''
  ];
});

$factory->defineAs(SIFMEDE\RelNewComment::class,'withNew',function($faker){
  $news = Noticia::all()->lists('id')->all();
  return [
    'new_id' => $faker->randomElement($news),
    'comment_id' => ''
  ];
});