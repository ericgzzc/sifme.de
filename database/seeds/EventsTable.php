<?php

use Illuminate\Database\Seeder;

class EventsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SIFMEDE\Evento::class,10)->create();
    }
}
