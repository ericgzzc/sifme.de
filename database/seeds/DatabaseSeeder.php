<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //Model::unguard();
        $this->call(ProductionSeed::class);
        /*$this->call(UserPoblation::class);
        $this->call(NoticiasTable::class);
        $this->call(EventsTable::class);
        $this->call(ContactInfo::class);
        $this->call(CommentsSeed::class);*/
        //Model::reguard();
    }
}
