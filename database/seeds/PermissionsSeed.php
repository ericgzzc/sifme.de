<?php

use Illuminate\Database\Seeder;
use SIFMEDE\User;

class PermissionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('permissions')->insert([
            'name' => 'de_admin',
            'description' => 'Administrador'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'de_add_users',
            'description' => 'Agregar Usuarios'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'de_edit_users',
            'description' => 'Editar Permisos'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'de_delete_users',
            'description' => 'Eliminar Usuarios'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'de_post_news',
            'description' => 'Agregar Noticias'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'de_post_events',
            'description' => 'Agregar Eventos'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'de_comments',
            'description' => 'Hacer Comentarios'
        ]);
    }
}