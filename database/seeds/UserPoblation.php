<?php

use Illuminate\Database\Seeder;
use SIFMEDE\User;

class UserPoblation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // 10 random clients
      factory(SIFMEDE\User::class,10)->create();
      // 5 random admin users
      factory(SIFMEDE\User::class,'admin',5)->create();
      // 1 random master user
      factory(SIFMEDE\User::class,'master',1)->create();
      
    }
}
