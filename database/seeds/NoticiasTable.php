<?php

use Illuminate\Database\Seeder;

class NoticiasTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(SIFMEDE\Noticia::class,10)->create();
      factory(SIFMEDE\Noticia::class,'deleted',10)->create();
      factory(SIFMEDE\Noticia::class,5)->make([
        'user_id' => 17
      ])->each(function($u){
        //DB::table('news')->insert([$u]);
        $u->save();
      });
    }
}
