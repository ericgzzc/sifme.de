<?php

use Illuminate\Database\Seeder;

class ContactInfo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contact_pages')->insert([
          'name' => 'SIFME',
          'email' => 'sifme@mail.com',
          'phone' => '(81) 12-32 3101',
          'street_address' => 'Eugenio Garza Sada',
          'number_address' => '2501',
          'city_address' => 'Monterrey',
          'state_address' => 'Nuevo Leon',
          'country_address' => 'México'
      ]);
    }
}
