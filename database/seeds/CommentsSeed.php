<?php

use Illuminate\Database\Seeder;

class CommentsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //creating comments for events
        factory(SIFMEDE\Comment::class,500)
        ->make()
        ->each(function($comment){
            $comment->save();
            $rel = factory(SIFMEDE\RelEventComment::class,'withEvent')
                ->make();
            $rel->comment_id = $comment->id;
            $rel->save();
        });
        
        factory(SIFMEDE\Comment::class,500)
        ->make()
        ->each(function($comment){
            $comment->save();
            $rel = factory(SIFMEDE\RelNewComment::class,'withNew')
                ->make();
            $rel->comment_id = $comment->id;
            $rel->save();
        });
        
    }
}
