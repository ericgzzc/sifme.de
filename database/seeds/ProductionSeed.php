<?php

use Illuminate\Database\Seeder;
use SIFMEDE\User;

class ProductionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsSeed::class);
        
        DB::table('users')->insert([
            'first_name' => 'Tania',
            'last_name' => 'Garrido Salido',
            'email' => 'taniagarridosalido@gmail.com',
            'password' => bcrypt('ainat')
        ]);
        
        User::find(1)->givePermissionTo('de_admin');
        
        DB::table('users')->insert([
            'first_name' => 'Jesus David ',
            'last_name' => 'De La Fuente Amaya',
            'email' => 'jdfa14@gmail.com',
            'password' => bcrypt('divad')
        ]);
        User::find(2)->givePermissionTo('de_admin');
        DB::table('users')->insert([
            'first_name' => 'Eric',
            'last_name' => 'Easydro',
            'email' => 'ericConC@gmail.com',
            'password' => bcrypt('123456')
        ]);
        User::find(3)->givePermissionTo('de_admin');
        
        DB::table('users')->insert([
            'first_name' => 'Eri_con_c',
            'last_name' => 'Easy_dro',
            'email' => 'test@gmail.com',
            'password' => bcrypt('123456')
        ]);
        User::find(4)->givePermissionTo('de_admin');
        //$this->call(RolesSeed::class);
        //$this->call(PermissionRolSeed::class);
    }
}

/*
$t = SIFMEDE\User::find(17);
$t->givePermissionTo('noticias admin');
$t->can('noticias admin');

*/