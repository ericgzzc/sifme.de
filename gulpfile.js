var gulp = require('gulp');
var elixir = require('laravel-elixir');
var conf = require('./gulp/config')
var wrench = require('wrench');
var wiredep = require('wiredep');
var $ = require('gulp-load-plugins')();
require('laravel-elixir-wiredep');


wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});

elixir(function (mix) {
  mix.bower()
  .clean()
  .buildLayouts()
  .buildFonts()
  .images()
  .styles([
    "*.css",
    "public/**/*.css"
  ], "public/css/public.css")
  .styles([
    "*.css",
    "admin/**/*.css"
  ], "public/css/admin.css")
  .scripts([
    'main/**/*.js',
    '*.js',
    '**/*.js',
    'admin/**/*.js',
  ], 'public/js/admin.js')
  .scripts([
    'main/**/*.js',
    '*.js',
    '**/*.js',
    'public/**/*.js',
  ], 'public/js/public.js')
  .scripts(wiredep(conf.wiredep.wireOpts).js.map(function (path) {
      return path.replace(__dirname, '');
    }),
    'public/js/vendor.min.js',
    'public')
  .styles(wiredep(conf.wiredep.wireOpts).css.map(function (path) {
      return path.replace(__dirname, '');
    }),
    'public/css/vendor.min.css',
    'public')
  .injection([
    'public/js/*.js',
    'public/css/*.css',
    '!public/js/public.js',
    '!public/css/public.css',
    '!public/js/admin.js',
    '!public/css/admin.css'
    ],
    ['resources/views/layouts/generic/main.blade.php']
    ,'resources/views/layouts/generic/'); 
});