<!DOCTYPE html>
<html lang="es" ng-app="sifme">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SIFME</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>


    <!-- Styles -->    

    <!-- bower:css -->
    <!-- endbower -->
    <!-- Elixir Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
    <!-- End elixir styles -->
    
</head>
<body id="app-layout" class="body-user">
    <nav class="navbar navbar-user">
        <div class="container container-user">
            <div class="navbar-header">
                <!-- Branding Image -->
                <a href="{{ url('/DE') }}">
                    <img class="logo" src='/images/logo_SF_sifme.png' />
                </a>

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>

            <div class="collapse navbar-collapse nav-user" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/Miembros') }}" class="a-user">Inicio</a></li>
                    <li><a href="{{ url('/Miembros/Informacion') }}" class="a-user">Información</a></li>
                    <li><a href="{{ url('/Miembros/MisionVision') }} " class="a-user">Misión y Visión</a></li>
                    <li><a href="{{ url('/Miembros/Noticias') }}" class="a-user">Noticias</a></li>
                    <li><a href="{{ url('/Miembros/Calendario') }}" class="a-user">Calendario</a></li>
                    <li><a href="{{ url('/Miembros/Donaciones') }}" class="a-user">Donaciones</a></li>
                    <li><a href="{{ url('/Miembros/Contacto') }}" class="a-user">Contacto</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}" class="a-user">Iniciar Sesión</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @if ( Session::has('flash_message') )
        <div class="col-md-6 col-md-offset-3 alert {{ Session::get('flash_type') }}">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>{{ Session::get('flash_message') }}</strong>
        </div>
        {{ Session::forget('flash_message') }}
        {{ Session::forget('flash_type') }}
    @endif
    @yield('content')

    
    <!-- bower:js -->
    <!-- endbower -->

    <!-- JavaScripts -->
    <script src="{{ URL::asset('js/app.js') }}" ></script>

    
    <!-- Google Maps API -->
    
    
</body>
</html>
