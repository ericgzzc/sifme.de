<!DOCTYPE html>
<html lang="es" ng-app="sifme">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title','SIFME')</title>
    <!-- local:styles:prebower -->
    @yield('l-pbstyles')
    <!-- endlocal -->
    
    <!-- bower:css -->
    <!-- endbower -->
    
    <!-- inject:css -->
    <!-- endinject -->
    
    <!-- local:styles -->
    @yield('l-styles')
    <!-- endlocal -->
    
    
</head>
<body class="@yield('bodyclass')">
    <!-- navbar -->
    @yield('l-navbar')
    <!-- end:navbar -->
    
    <!-- contentr -->    
    @yield('l-content')
    <!-- end:content -->
    
    <!-- local:scrips:prebower -->
    @yield('l-pbscripts')
    <!-- endlocal -->
    
    <!-- bower:js -->
    <!-- endbower -->
    
    <!-- inject:js -->
    <!-- endinject -->
    
    <!-- local:scrips -->
    @yield('l-scripts')
    <!-- endlocal -->
    
    <!-- Google Maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDClODaEgTNvlu7BornNSSlwem1rrgLdIA&libraries=places"></script>
</body>
</html>