<!DOCTYPE html>
<html lang="es" ng-app="sifme">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SIFME</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->    
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    <!-- bower:css -->
    <!-- endbower -->
    <!-- Elixir Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/admin.css') }}" />
    <!-- End elixir styles -->
    <!-- angular -->
    <!-- endangular -->
</head>
<body id="app-layout">
      
    @yield('layout','NO LAYOUT')
    
    <!-- bower:js -->
    <!-- endbower -->

    <!-- JavaScripts -->
    <script src="{{ URL::asset('js/admin.js') }}" ></script>
    @yield('scripts','')
    
    <!-- Google Maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDClODaEgTNvlu7BornNSSlwem1rrgLdIA&libraries=places"></script>
</body>
</html>