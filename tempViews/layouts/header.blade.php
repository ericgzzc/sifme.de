<!DOCTYPE html>
<html lang="es" ng-app="sifme">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SIFME</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->    
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    <!-- bower:css -->
    <!-- endbower -->
    <!-- Elixir Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
    <!-- End elixir styles -->
    
</head>
<body id="app-layout">
    <nav class="navbar navbar-user">
        <div class="container container-user">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    SIFME
                </a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Inicio</a></li>
                    <li><a href="{{ url('/home') }}">Información</a></li>
                    <li><a href="{{ url('/Miembros') }}">Miembros</a></li>
                    <li><a href="{{ url('/Noticias') }}">Noticias</a></li>
                    <li><a href="{{ url('/Eventos') }}">Eventos</a></li>
                    <li><a href="{{ url('/Donaciones') }}">Donaciones</a></li>
                    <li><a href="{{ url('/Contacto') }}">Contacto</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Iniciar Sesión</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            
        </div>
    </nav>

    @if ( Session::has('flash_message') )
        <div class="col-md-6 col-md-offset-3 alert {{ Session::get('flash_type') }}">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>{{ Session::get('flash_message') }}</strong>
        </div>
        {{ Session::forget('flash_message') }}
        {{ Session::forget('flash_type') }}
    @endif
    @yield('content')

    
    <!-- bower:js -->
    <!-- endbower -->

    <!-- JavaScripts -->
    <script src="{{ URL::asset('js/app.js') }}" ></script>
    
    <!-- Google Maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDClODaEgTNvlu7BornNSSlwem1rrgLdIA&callback=initMap"></script>
    
    
</body>
</html>
