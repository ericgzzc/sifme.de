<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">

      <!-- Collapsed Hamburger -->
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <!-- Branding Image -->
      <a class="navbar-brand" href="{{ url('/') }}">
                    SIFME
                </a>
    </div>

    <div class="collapse navbar-collapse" id="app-navbar-collapse">
      <!-- Left Side Of Navbar -->
      <ul class="nav navbar-nav">
        @if(Auth::user()->hasPermissionTo("de_admin"))
        <li><a href="{{  route('admin.home') }}">Inicio</a></li>
        @endif
        @if(Auth::user()->hasPermissionTo("de_admin") || Auth::user()->hasPermissionTo('de_edit_info'))
        <li><a href="{{  route('admin.information.home') }}">Información</a></li>
        @endif
        @if(Auth::user()->hasPermissionTo("de_admin") || Auth::user()->hasPermissionTo('de_edit_users') || Auth::user()->hasPermissionTo('de_delete_users') || Auth::user()->hasPermissionTo('de_add_users'))
        <li><a href="{{  route('admin.members.home') }}">Miembros</a></li>
        @endif
        @if(Auth::user()->hasPermissionTo("de_admin") || Auth::user()->hasPermissionTo('de_post_news'))
        <li><a href="{{  route('admin.news.home') }}">Noticias</a></li>
        @endif
        @if(Auth::user()->hasPermissionTo("de_admin") || Auth::user()->hasPermissionTo('de_post_events'))
        <li><a href="{{  route('admin.events.home') }}">Eventos</a></li>
        @endif
        @if(Auth::user()->hasPermissionTo("de_admin") || Auth::user()->hasPermissionTo('de_edit_info'))
        <li><a href="{{  route('admin.donations.home') }}">Donaciones</a></li>
        @endif
        @if(Auth::user()->hasPermissionTo("de_admin") || Auth::user()->hasPermissionTo('de_edit_info'))
        <li><a href="{{  route('admin.contact.home') }}">Contacto</a></li>
        @endif
      </ul>

      <!-- Right Side Of Navbar -->
      <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links -->
        @if (Auth::guest())
        <li><a href="{{ route('public.login') }}">Iniciar Sesión</a></li>
        @else
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::user()->first_name }}
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ route('member.home') }}"><i class="fa fa-btn fa-object-group" aria-hidden="true"></i>Cambiar vista</a></li>
            <li><a href="{{ route('public.logout') }}"><i class="fa fa-btn fa-sign-out" aria-hidden="true"></i>Cerrar sesión</a></li>
          </ul>
        </li>
        @endif
      </ul>
    </div>
  </div>
</nav>