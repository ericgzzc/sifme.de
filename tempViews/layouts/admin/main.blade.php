@extends('layouts.generic.main')

@section('l-title')
  @yield('title','SIFME Admin')
@endsection

@section('l-styles')
  @yield('styles','')
  <link rel="stylesheet" href="{{ URL::asset('css/admin.css') }}" />
@endsection

@section('l-pbstyles')
  @yield('l-pbstyles','')
@endsection

@section('l-scripts')
  <script src="{{ URL::asset('js/admin.js') }}" ></script>
  @yield('scripts','')
@endsection

@section('l-pbscripts')
  @yield('pbscripts','')
@endsection

@section('l-content')
  @yield('content','')
@endsection

@section('l-navbar')
  @include('layouts.admin.navbar')
@endsection
