@extends('layouts.generic.main')

@section('l-title')
  @yield('title','SIFME User')
@endsection

@section('l-styles')
  @yield('styles','')
  <link rel="stylesheet" href="{{ URL::asset('css/public.css') }}" >
@endsection

@section('l-pbstyles')
  @yield('pbstyles','')
@endsection

@section('bodyclass')
  body-user
@endsection

@section('l-scripts')
  <script src="{{ URL::asset('js/public.js') }}" ></script>
  @yield('scripts','')
@endsection

@section('l-pbscripts')
  @yield('pbscripts','')
@endsection

@section('l-content')
  @yield('content','')
@endsection

@section('l-navbar')
  @include('layouts.public.navbar')
@endsection
