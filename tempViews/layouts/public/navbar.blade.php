<nav class="navbar navbar-user">
        <div class="container container-user">
            <div class="navbar-header">
                <!-- Branding Image -->
                <a href="{{ route('member.home') }}">
                    <img class="logo" src='/images/logo_SF_sifme.png' />
                </a>

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>

            <div class="collapse navbar-collapse nav-user" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->

                <ul class="nav navbar-nav">
                    @if(!Auth::guest())
                    <li><a href="{{ route('member.home') }}" class="a-user">Inicio</a></li>
                    <li><a href="{{ route('member.information.home') }}" class="a-user">Información</a></li>
                    <li><a href="{{ route('member.missionvission.home') }} " class="a-user">Misión y Visión</a></li>
                    <li><a href="{{ route('member.news.home') }}" class="a-user">Noticias</a></li>
                    <li><a href="{{ route('member.events.calendar') }}" class="a-user">Calendario</a></li>
                    <li><a href="{{ route('member.donation.home') }}" class="a-user">Donaciones</a></li>
                    <li><a href="{{ route('member.contact.home') }}" class="a-user">Contacto</a></li>
                    @else
                    <li><a href="{{ route('public.home') }}" class="a-user">Inicio</a></li>
                    <li><a href="{{ route('public.information.home') }}" class="a-user">Información</a></li>
                    <li><a href="{{ route('public.missionvission.home') }} " class="a-user">Misión y Visión</a></li>
                    <li><a href="{{ route('public.news.home') }}" class="a-user">Noticias</a></li>
                    <li><a href="{{ route('public.calendar.home') }}" class="a-user">Calendario</a></li>
                    <li><a href="{{ route('public.contact.home') }}" class="a-user">Contacto</a></li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('public.login') }}" class="a-user">Iniciar Sesión</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle a-user" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->first_name }} <span class="caret a-user"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @if(Auth::user()->hasPermissionTo('de_admin'))
                                <li><a href="{{ route('admin.home') }}"><i class="fa fa-btn fa-object-group" aria-hidden="true"></i>Cambiar vista</a></li>
                                @endif
                                <li><a href="{{ route('public.logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>