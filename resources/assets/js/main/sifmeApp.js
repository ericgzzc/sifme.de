
(function(){
  'use strict'
  var app = angular.module('sifme', [
    'ngStorage',
    'ngValidate',
    'angular-linq',
    'datatables',
    'ui.bootstrap',
    'frapontillo.bootstrap-switch',
    'toastr'
  ],
  interpolator);
  
  app.factory('httpInterceptor',httpInterceptor)
    .config(httpProvider);
  
  
  interpolator.$inject = ['$interpolateProvider'];
  
  function interpolator($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
  }
  
  httpInterceptor.$inject = ['$q','$location','$window'];
  
  function httpInterceptor($q,$location,$window){
    
    return {
      request: request,
      requestError: requestError,
      response: response,
      responseError: responseError
    }
    
    function request(config){
      //console.log('request',config);
      config.headers = {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        };
      return config;
    }
    
    function requestError(error){
      //console.log('requestError',error);
      return $q.reject(error);
    }
    
    function response(config){
      //console.log('respose',config);
      return config;
    }
    
    function responseError(error){
      //console.log('responseError',error);
      return $q.reject(error);
    }
  }
  
  httpProvider.$inject = ['$httpProvider'];
  
  function httpProvider($httpProvider){
    $httpProvider.interceptors.push('httpInterceptor');
  }
  
})();