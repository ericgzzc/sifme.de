// public/js/services/commentService.js

(function(){
    angular.module('sifme').factory('enviarCorreoService', enviar) 

    enviar.$inject = ["$q","$http"]

    function  enviar($q,$http)
        {
            return {
                get : function(evento_id) {
                    return $http.get('/Miembros/Contacto/usuario');
                },
                // get all the comments
                sendEmail : function(data) {
                    return $http({
                            method: 'POST',
                            url: '/Miembros/Contacto/sendEmail',
                            data: data
                  });
                },
            }
        } 

})();