(function () {
    angular.module('sifme').factory('seguirService', asistir)

    asistir.$inject = ["$q", "$http"]

    function asistir($q, $http) {
        return {
            // get all the comments
            follow: function (noticia_id) {
                return $http.get('/Miembros/Noticia/seguir/' + noticia_id);
            },
            get: function () {
                return $http.get('/Recursos/Noticias/all');
            },
            unfollow: function (noticia_id) {
                return $http.get('/Miembros/Noticia/delete/' + noticia_id);
            },
            existe: function (noticia_id) {
                return $http.get('/Miembros/Noticia/existe/' + noticia_id);
            },
        }
    }

})();