// public/js/services/commentService.js

(function(){
    angular.module('sifme').factory('asistirService', asistir) 

    asistir.$inject = ["$q","$http"]

    function  asistir($q,$http)
        {
            return {
                // get all the comments
                asistir : function(evento_id) {
                    return $http.get('/Miembros/Evento/asistir/'+evento_id);
                },
            }
        } 

})();