// public/js/services/commentService.js

(function(){
    angular.module('sifme').factory('commentServiceEvento', addComment) 

    addComment.$inject = ["$q","$http"]

    function  addComment($q,$http)
        {
            return {
                // get all the comments
                get : function(evento_id) {
                    return $http.get('/Miembros/Evento/Comentarios/'+evento_id);
                },

                // save a comment (pass in comment data)
                save : function(commentData) {
                    return $http({
                        method: 'POST',
                        url: '/Miembros/Evento/'+evento_id,
                        data: commentData
                    });
                },

                // destroy a comment
                destroy : function(id, id_rel) {
                    if($http.delete('/Miembros/Evento/Comentario_Evento/'+id_rel))
                    return $http.delete('/Miembros/Evento/Comentario/'+id);
                },

                asistir : function(evento_id) {
                    return $http.get('/Miembros/Evento/asistir/'+evento_id);
                },

                noasistir : function(evento_id) {
                    return $http.get('/Miembros/Evento/no_asistir/'+evento_id);
                },

                existe : function(evento_id) {
                    return $http.get('/Miembros/Evento/existe/'+evento_id);
                }


            }
        } 

})();