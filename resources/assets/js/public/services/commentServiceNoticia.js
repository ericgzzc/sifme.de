// public/js/services/commentService.js

(function(){
    angular.module('sifme').factory('commentServiceNoticia', addComment) 

    addComment.$inject = ["$q","$http"]

    function  addComment($q,$http)
        {
            return {
                // get all the comments
                get : function(noticia_id) {
                    return $http.get('/Miembros/Noticia/Comentarios/'+noticia_id);
                },

                // save a comment (pass in comment data)
                save : function(commentData) {
                    return $http({
                        method: 'POST',
                        url: '/Miembros/Noticia/'+noticia_id,
                        data: commentData
                    });
                },

                // destroy a comment
                destroy : function(id, id_rel) {
                    if($http.delete('/Miembros/Noticia/Comentario_Noticia/'+id_rel))
                    return $http.delete('/Miembros/Noticia/Comentario/'+id);
                }
            }
        } 

})();