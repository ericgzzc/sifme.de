(function(){
  'use strict'

  angular.module('sifme')
    .controller('contactController',contactController);
    
  contactController.$inject = [];
  
  function contactController(){
    var vm = this;
       
     activate();
    function activate(){
      $('.flexslider').flexslider();
      initMap();
      if( $("#inputStreet").val() != "" && $("#inputNumber").val() != "" && $("#inputCity").val() != "" && $("#inputCountry").val() != "" )
         $("#verMapa")[0].click();
    }
  }
})();