(function(){
  'use strict'

  angular.module('sifme')
    .controller('enviarCorreoControler',enviarCorreoControler);
    
  enviarCorreoControler.$inject = ["$scope", "$http", "enviarCorreoService","$uibModal", 'toastr'];
  
  function enviarCorreoControler($scope, $http, enviarCorreoService, $uibModal, toastr){
    var vm = this;
   

    $scope.mensajeData = {};


      enviarCorreoService.get()
        .success(function(data) {
            $scope.mensajeData.name = data.first_name;
            $scope.mensajeData.email = data.email;
        });

      $scope.enviarCorreo = function() {
        enviarCorreoService.sendEmail($scope.mensajeData)
            .success(function(data) {
              toastr.info('El Correo se ha enviado Existosamente', 'Información');
                $scope.mensajeData = [];
            })
            .error(function(data) {
                console.log(data);
            });
    };
      activate();
    function activate(){
      $('.flexslider').flexslider();
      initMap();
      if( $("#inputStreet").val() != "" && $("#inputNumber").val() != "" && $("#inputCity").val() != "" && $("#inputCountry").val() != "" )
         $("#verMapa")[0].click();
    }

  }
})();