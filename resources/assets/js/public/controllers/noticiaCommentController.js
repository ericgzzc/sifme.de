(function () {
    'use strict';

    angular.module('sifme')
        .controller('noticiaCommentController', noticiaCommentController);

    noticiaCommentController.$inject = ["$scope", "$http", "commentServiceNoticia", "$uibModal", "seguirService"];

    function noticiaCommentController($scope, $http, commentServiceNoticia, $uibModal, seguirService) {
        var vm = this;

        $scope.commentData = {};

        seguirService.existe(noticia_id)
            .success(function (getData) {
                if (getData.success)
                    return $scope.existe = true;
                else
                    return $scope.existe = false;
            });
        commentServiceNoticia.get(noticia_id)
            .success(function (data) {
                $scope.comments = data;

            });
        $scope.submitComment = function () {

            commentServiceNoticia.save($scope.commentData)
                .success(function (data) {
                    commentServiceNoticia.get(noticia_id)
                        .success(function (getData) {
                            $scope.comments = getData;
                            console.log("entra");
                        });
                    $scope.commentData.comment = "";

                })
                .error(function (data) {
                    console.log(data);
                });
        };

        $scope.deleteComment = function (id, id_rel) {

            commentServiceNoticia.destroy(id, id_rel)
                .success(function (data) {

                    commentServiceNoticia.get(noticia_id)
                        .success(function (getData) {
                            $scope.comments = getData;
                        });

                });
        };

        $scope.followNew = function (noticia_id) {

            seguirService.follow(noticia_id)
                .success(function (data) {
                    seguirService.existe(noticia_id)
                        .success(function (getData) {
                            if (getData.success)
                                return $scope.existe = true;
                            else
                                return $scope.existe = false;
                        });
                })
                .error(function (data) {

                });
        };

        $scope.unfollowNew = function (noticia_id) {

            seguirService.unfollow(noticia_id)
                .success(function (data) {
                    seguirService.existe(noticia_id)
                        .success(function (getData) {
                            if (getData.success)
                                return $scope.existe = true;
                            else
                                return $scope.existe = false;
                        });

                })
                .error(function (data) {

                });
        };


        activate();
        function activate() {
            setTimeout(function () {
                $('.flexslider').flexslider();
            }, 1000);

        }
    }
})();