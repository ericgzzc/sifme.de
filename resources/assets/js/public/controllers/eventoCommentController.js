(function () {
    'use strict'

    angular.module('sifme')
        .controller('eventoCommentController', eventoCommentController);

    eventoCommentController.$inject = ["$scope", "$http", "commentServiceEvento", "$uibModal"];

    function eventoCommentController($scope, $http, commentServiceEvento, $uibModal) {
        var vm = this;


        $scope.commentData = {};

        commentServiceEvento.existe(evento_id)
            .success(function (getData) {
                if (getData.success)
                    return $scope.existe = true;
                else
                    return $scope.existe = false;
            });
            
        commentServiceEvento.get(evento_id)
            .success(function (data) {
                $scope.comments = data;
            });
        $scope.submitComment = function () {
            commentServiceEvento.save($scope.commentData)
                .success(function (data) {
                    $scope.commentData.comment = "";
                    commentServiceEvento.get(evento_id)
                        .success(function (getData) {
                            $scope.comments = getData;
                            console.log("entra");
                        });

                })
                .error(function (data) {
                    console.log(data);
                });
        };


        $scope.asistir = function (id) {

            commentServiceEvento.asistir(evento_id)
                .success(function (getData) {

                    commentServiceEvento.existe(evento_id)
                        .success(function (getData) {
                            if (getData.success)
                                return $scope.existe = true;
                            else
                                return $scope.existe = false;
                        });

                });

        };




        $scope.noasistir = function (id) {

            commentServiceEvento.noasistir(evento_id)
                .success(function (getData) {
                    commentServiceEvento.existe(evento_id)
                        .success(function (getData) {
                            if (getData.success)
                                return $scope.existe = true;
                            else
                                return $scope.existe = false;
                        });

                });

        };

        $scope.deleteComment = function (id, id_rel) {

            commentServiceEvento.destroy(id, id_rel)
                .success(function (data) {

                    commentServiceEvento.get(evento_id)
                        .success(function (getData) {
                            $scope.comments = getData;
                        });

                });
        };


        activate();
        function activate() {


        }
    }
})();