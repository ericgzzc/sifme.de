(function(){
  'use strict'

  angular.module('sifme')
    .controller('noticiasController',noticiasController);
    
  noticiasController.$inject = ["$scope", "$http", "seguirService","$uibModal"];
  
  function noticiasController($scope, $http, seguirService, $uibModal){
    var vm = this;

   
        seguirService.get()
            .success(function(data) {
                $scope.noticias = data;
            })
            .error(function(data) {
            
            });



   
    $scope.followNew = function(noticia_id) {

        seguirService.follow(noticia_id)
            .success(function(data) {
                  seguirService.get()
                        .success(function(data) {
                            $scope.noticias = data;
                        })
                        .error(function(data) {
                        
                    });
                setTimeout(function(){
                    $('.flexslider').flexslider();      
                }, 500);
            })
            .error(function(data) {
            
            });
    };

    $scope.unfollowNew = function(noticia_id) {

        seguirService.unfollow(noticia_id)
            .success(function(data) {
                  seguirService.get()
                    .success(function(data) {
                        $scope.noticias = data;
                    })
                    .error(function(data) {
                
            });
            setTimeout(function(){
                $('.flexslider').flexslider();      
            }, 500);

            })
            .error(function(data) {
            
            });
    };

      
     activate();
    function activate(){
        setTimeout(function(){
            $('.flexslider').flexslider();      
        }, 1000);
    }
  }
})();