function initMap() {
  if (document.getElementById('map')) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: { lat: 25.6667, lng: -100.3167 }

    });
    var geocoder = new google.maps.Geocoder();

    document.getElementById('verMapa').addEventListener('click', function () {
      var address = $("#inputNumber").val() + " " + $("#inputStreet").val() + ", " + $("#inputCity").val() + " " + $("#inputCountry").val();
      geocodeAddress(geocoder, map, address);
    });
  }
}

function geocodeAddress(geocoder, resultsMap, address) {

  geocoder.geocode({ 'address': address }, function (results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      resultsMap.setCenter(results[0].geometry.location);

      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location,
        clickable: true
      });


marker.info = new google.maps.InfoWindow({
  content: '<b>Speed:</b> '
});

google.maps.event.addListener(marker, 'click', function() {
  marker.info.open(map, marker);
});

    } else {
      sweetAlert("Oops...", "La dirección " + address + " no se encontró en el mapa. Asegurese de que esté bien escrita.", "error");
    }
  });
}

function initAutocomplete() {
  if (document.getElementById('map_event')) {
    var map = new google.maps.Map(document.getElementById('map_event'), {
      center: { lat: 25.6667, lng: -100.3167 },
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var address = $("#address").val();
    var geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, map, address);

    google.maps.event.addDomListener(window, "resize", function () {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('address');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function (place) {
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
  }
}

(function () {

  $('select').select2({
    theme: "bootstrap"
  });
  
})();

var opcionesDZ = {
  paramName: "file",
  acceptedFiles: "image/*",
  addRemoveLinks: true,
  autoProcessQueue: false,
  uploadprogress: function (progress, bytesSent) {
    console.log(progress);
    console.log('12');
  },
  sending: function (file, xhr, formData) {
    formData.append("_token", $('[name=_token').val());
  },
  dictResponseError: 'Error al subir el archivo',
  dictCancelUpload: 'Cancelar el archivo',
  dictCancelUploadConfirmation: '¿Está seguro?',
  dictRemoveFile: 'Eliminar de la lista',
  dictDefaultMessage: "Arrastre los archivos que desee adjuntar aquí",
  dictInvalidFileType: 'No se permite éste tipo de archivos',
  clickable: true,
  parallelUploads: 100
};
