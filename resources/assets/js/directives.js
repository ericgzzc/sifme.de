(function () {
  angular.module('sifme')
    .directive('dropzone', dropzone);

  function dropzone() {
    return {
      restrict: 'C',
      requiere: ['^ngUrl','^ngToken','^ngSubmit'],
      scope: {
        ngUrl: '=',
        ngToken: '=',
        ngSubmit: '='
      },
      link: function (scope, element, attrs) {
        var config = {
          url: scope.ngUrl ? scope.ngUrl : '#',
          paramName: "file",
          acceptedFiles: "image/*",
          addRemoveLinks: true,
          maxThumbnailFilesize: 10,
          autoProcessQueue: false,
          dictResponseError: 'Error al subir el archivo',
          dictCancelUpload: 'Cancelar el archivo',
          dictCancelUploadConfirmation: '¿Está seguro?',
          dictRemoveFile: 'Eliminar de la lista',
          dictDefaultMessage: "Arrastre los archivos que desee adjuntar aquí",
          dictInvalidFileType: 'No se permite éste tipo de archivos',
          clickable: true,
          parallelUploads: 100,
          sending: function (file, xhr, formData) {
            formData.append("_token", scope.ngToken);
          },
        }
        var dfd;
        var baseUrl = scope.ngUrl;
        var eventHandlers = {
          'addedfile': function (file) {
            scope.file = file;
            //if (this.files[1] != null) {
            //  this.removeFile(this.files[0]);
            //}
            scope.$apply(function () {
              scope.fileAdded = true;
            });
          },

          'success': function (file, response) {
          },
          'queuecomplete': function(){
            dfd.resolve();
          }
        }
        
        dropzone = new Dropzone(element[0], config);

        angular.forEach(eventHandlers, function (handler, event) {
          dropzone.on(event, handler);
        });

        scope.ngSubmit = function (id) {
          dfd = $.Deferred();
          dropzone.options.url = baseUrl + id;
          dropzone.processQueue();
          return dfd.promise();
        };

        scope.resetDropzone = function () {
          dropzone.removeAllFiles();
        }
      }
    }
  }

})();