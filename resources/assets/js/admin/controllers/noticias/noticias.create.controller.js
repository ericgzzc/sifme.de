(function(){
  'use strict';
  
  angular.module('sifme')
    .controller('newsCreateController',newsCreateController);
    
  newsCreateController.$inject = ['$uibModalInstance', 'newsService','toastr','$scope'];
    
  function newsCreateController($uibModalInstance, newsService,toastr,$scope){
    var vm = this;
    vm.actions = {
      close: close,
      submit: submit
    };
    
    vm.defaults = {
      url: '/Recursos/Archivos/Noticia/',
      title: 'Crear Noticia',
      validationOptions: {
        rule: {
          title: {
            required: true
          },
          content: {
            required: true
          }
        },
        messages: {
          title: {
            required: 'Campo de titulo requerido'
          },
          content: {
            required: 'El contenido es requerido'
          }
        }
      }
    };
    
    vm.form = {
      title: "",
      content: "",
      sector: "",
      public: false
    }
    
    function save(){
      newsService.addNews(vm.form)
        .then(function(response){
          if($('.dropzone .dz-preview').length > 0){
            
            vm.actions.upload(response.data.id)
              .then(function(){
                $uibModalInstance.close(response.data);
              });
          }else{
            $uibModalInstance.close(response.data);
          } 
        },function(error){
          console.log('Error',error);
        });
    }
    
    function submit(form){
      if(form.validate()){
        save();
      }else{
        toastr.info('Favor de llenar los campos faltantes');
      }
    }
    
    function close(){
      $uibModalInstance.dismiss('close');
    }
  }
})();