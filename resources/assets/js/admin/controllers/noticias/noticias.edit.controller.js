(function(){
  'use strict';
  
  angular.module('sifme')
    .controller('newsEditController',newsEditController);
    
  newsEditController.$inject = ['$uibModalInstance', 'newsService','news'];
    
  function newsEditController($uibModalInstance, newsService,news){
    var vm = this;
    
    vm.actions = {
      close: close,
      save: save,
      submit: submit,
      deleteIt: deleteIt
    }
    
    vm.form = {
      title: news.title,
      content: news.content,
      public: news.public == 1,
      sector: news.sector,
      files: []
    }
    
    vm.defaults = {
      url: '/Recursos/Archivos/Noticia/',
      title: 'Editar Noticia: ' + news.title,
      validationOptions: {
        rule: {
          title: {
            required: true
          },
          content: {
            required: true
          }
        },
        messages: {
          title: {
            required: 'Campo de titulo requerido'
          },
          content: {
            required: 'El contenido es requerido'
          }
        }
      }
    };
    
    activate();
    
    function save(){
      newsService.editNews(vm.form,news.id)
        .then(function(response){
          if($('.dropzone .dz-preview').length > 0){
            vm.actions.upload(news.id)
              .then(function(){
                $uibModalInstance.close(response.data)
              });
          }else{
            $uibModalInstance.close(response.data);
          }
        },function(error){
          console.log('Error',error);
        });
    }
    
    
    function activate(){
      newsService.getFiles(news.id)
        .then(function(response){
          vm.form.files = response.data;
        },function(error){
          console.log(error);
        });
    }
    
    function deleteIt(){
      
    }
    
    function close(){
      $uibModalInstance.dismiss('close');
    }
    
    function submit(form){
      if(form.validate()){
        save();
      }else{
        toastr.info('Favor de llenar los campos faltantes');
      }
    }
  }
})();