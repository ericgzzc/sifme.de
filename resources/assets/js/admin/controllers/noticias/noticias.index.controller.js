(function () {
  'use strict';

  angular.module('sifme')
    .controller('noticiasIndexController', noticiasIndexController);

  noticiasIndexController.$inject = ['modalService', 'newsService', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$uibModal','toastr'];

  function noticiasIndexController(modalService, newsService, DTOptionsBuilder, DTColumnDefBuilder, $uibModal,toastr) {
    var vm = this;

    vm.options = {
      dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers'),
      dtColumnDefs: [
        DTColumnDefBuilder.newColumnDef(0),               // ID
        DTColumnDefBuilder.newColumnDef(1),               // First Name
        DTColumnDefBuilder.newColumnDef(2),               // Last Name
        DTColumnDefBuilder.newColumnDef(3)                // email
      ]
    };

    vm.actions = {
      addNews: addNews,
      editNews: editNews,
      deleteNews: deleteNews
    }

    vm.data = {
      news: []
    }

    activate();

    function activate() {
      newsService.getNews()
        .then(function (response) {
          vm.data.news = response.data;
        });
    }

    function addNews() {
      modalService.newsModal()
        .then(function(noticia){
          toastr.info('Noticia creada: ' + noticia.title);
          vm.data.news.push(noticia);
        });
    }

    function editNews(news) {
      modalService.newsModal(news)
        .then(function(response){
          console.log(response);
          var index = vm.data.news.indexOf(news);
          vm.data.news[index] = response;
        });
    }

    function deleteNews(news) {
      swal({
        title: "¡Eliminar noticia!",
        text: "¿Está seguro de querer eliminar la noticia " + news.title + "?",
        type: "warning",
        confirmButtonText: "Si, Eliminar!",
        cancelButtonText: "No, Cancelar!",
        showCancelButton: true
      }, function (response) {
        if (response) {
          newsService.deleteNews(news.id)
            .then(function (response) {
              console.log('here');
              _.remove(vm.data.news, function (n) { return n.id == response.data.id });
            }, function (error) {
              console.log('error', error);
            });
        }
      });
    }
  }
})();