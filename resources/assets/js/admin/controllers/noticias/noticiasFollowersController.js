(function () {
  'use strict';

  angular.module('sifme')
    .controller('noticiasFollowersController', noticiasFollowersController);

  noticiasFollowersController.$inject = ['$scope', '$http', 'newsService', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'toastr'];

  function noticiasFollowersController($scope, $http, newsService, DTOptionsBuilder, DTColumnDefBuilder, toastr) {
    var vm = this;

    vm.options = {
      dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers'),
      dtColumnDefs: [
        DTColumnDefBuilder.newColumnDef(0),               // First Name
        DTColumnDefBuilder.newColumnDef(1),               // Correo
      ]
    };

    vm.data = {
      news: []
    }

      $scope.mensajeData = {};

   

      $scope.enviarCoreo = function() {
        newsService.sendEmail($scope.mensajeData)
            .success(function(data) {
              toastr.info('El Correo se ha enviado Existosamente', 'Información');
              $scope.mensajeData = [];
            })
            .error(function(data) {
                console.log(data);
            });
    };

    activate();
    function activate() {
      newsService.getFollowers(eventoid)
        .then(function (response) {
          vm.data.users = response.data;
        });



    }
  }
})();