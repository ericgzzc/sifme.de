(function(){
  angular.module('sifme')
    .controller('contactController',contactController);
    
  contactController.$inject = [];
  
  function contactController(){
    var vm = this;
    
    activate();
    
    function activate(){
    initMap();
      if( $("#inputStreet").val() != "" && $("#inputNumber").val() != "" && $("#inputCity").val() != "" && $("#inputCountry").val() != "" )
    	   $("#verMapa")[0].click();
    }
  }
})();