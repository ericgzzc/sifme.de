(function () {
  'use strict';

  angular.module('sifme')
    .controller('eventosAsistentesController', eventosAsistentesController);

  eventosAsistentesController.$inject = ['$scope', '$http', 'eventosAttendingService', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'toastr'];

  function eventosAsistentesController($scope, $http, eventosAttendingService, DTOptionsBuilder, DTColumnDefBuilder, toastr) {
    var vm = this;

    vm.options = {
      dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers'),
      dtColumnDefs: [
        DTColumnDefBuilder.newColumnDef(0),               // First Name
        DTColumnDefBuilder.newColumnDef(1),               // Correo
      ]
    };

    vm.data = {
      news: []
    }

      $scope.mensajeData = {};

   

      $scope.enviarCoreo = function() {
        eventosAttendingService.sendEmail($scope.mensajeData)
            .success(function(data) {
              toastr.info('El Correo se ha enviado Existosamente', 'Información');
                $scope.mensajeData = [];
            })
            .error(function(data) {
                console.log(data);
            });
    };

    activate();
    function activate() {
      eventosAttendingService.getFollowers(eventoid)
        .then(function (response) {
          vm.data.users = response.data;
        });



    }
  }
})();