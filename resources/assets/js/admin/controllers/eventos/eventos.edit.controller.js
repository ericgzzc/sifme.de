(function(){
  'use strict';
  
  angular.module('sifme')
    .controller('eventosEditController',eventosEditController);
    
  eventosEditController.$inject = ['$uibModalInstance', 'eventService','events'];
    
  function eventosEditController($uibModalInstance, eventService,events){
    var vm = this;

    vm.form = {
      title: events.title,
      content: events.content,
      public: events.public == 1,
      address: events.address,
      start_date: events.start_date,
      end_date: events.end_date,
    }
    
    vm.actions = {
      close: close,
      save: save,
      deleteIt: deleteIt,
      submit: submit
    }

    vm.defaults = {
      title: 'Editar evento',
      validationOptions: {
        rule: {
          title: {
            required: true
          },
          content: {
            required: true
          }
        },
        messages: {
          title: {
            required: 'Campo de titulo requerido'
          },
          content: {
            required: 'El contenido es requerido'
          }
        }
      }
    }

    
    function save(){
      eventService.editEvent(vm.form,events.id)
        .then(function(response){
          if($('.dropzone .dz-preview').length > 0){
            vm.actions.upload(events.id)
              .then(function(){
                $uibModalInstance.close(response.data)
              });
          }else{
            $uibModalInstance.close(response.data);
          }
        },function(error){
          console.log('Error',error);
        });
      
    }
    
    function deleteIt(){
      
    }
    
    function close(){
      $uibModalInstance.dismiss('close');
    }

     function submit(form){
      if(form.validate()){
        save();
      }else{
        toastr.info('Favor de llenar los campos faltantes');
      }
    }

    activate();
    
    function activate(){
    }
  }
})();