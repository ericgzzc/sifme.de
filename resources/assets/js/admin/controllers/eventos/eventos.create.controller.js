var msas;

(function(){
  angular.module('sifme')
    .controller('eventosCreateController',eventosCreateController)
    
  eventosCreateController.$inject = ['$uibModalInstance','eventService'];  
    
  function eventosCreateController($uibModalInstance,eventService){
    var vm = msas = this;
    
    vm.form = {
      title: '',
      content: '',
      address: '',
      public: false
    };
    
    vm.actions = {
      close: close,
      submit: submit
    }
    
    vm.defaults = {
      title: 'Crear evento'
    }
    activate();
    
    function activate(){
      setTimeout(function(){
        $('#start_date').datetimepicker({
          locale: 'es',
          defaultDate: new Date()
        });
        $("#end_date").datetimepicker({
          locale: 'es',
          defaultDate: new Date()
        });
      },1000);
    }
    
    function save(){
      vm.form.end_date = $('#end_date').val();
      vm.form.start_date = $('#start_date').val();
      
      eventService.addEvent(vm.form)
        .then(function(response){
          $uibModalInstance.close(response.data);
        },function(error){
          console.log(error);
        });
    }
    
    function close(){
      $uibModalInstance.dismiss('close');
    }
    
    function submit(form){
      save();
    }
  }
})();