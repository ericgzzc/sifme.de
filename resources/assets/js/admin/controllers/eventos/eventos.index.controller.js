(function () {
  'use strict';

  angular.module('sifme')
    .controller('eventosIndexController', eventosIndexController);

  eventosIndexController.$inject = ['modalService', 'eventService', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$uibModal'];

  function eventosIndexController(modalService, eventService, DTOptionsBuilder, DTColumnDefBuilder, $uibModal) {
    var vm = this;

    vm.options = {
      dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers'),
      dtColumnDefs: [
        DTColumnDefBuilder.newColumnDef(0),               // ID
        DTColumnDefBuilder.newColumnDef(1),               // First Name
        DTColumnDefBuilder.newColumnDef(2),               // Last Name
        DTColumnDefBuilder.newColumnDef(3)                // email
      ]
    };

    vm.actions = {
      addEvent: addEvent,
      editEvent: editEvent,
      deleteEvent: deleteEvent
    }

    vm.data = {
      events: []
    }

    activate();

    function activate() {
      eventService.getEvent()
        .then(function (response) {
          vm.data.events = response.data;
        });
    }

    function addEvent() {
      modalService.eventModal()
        .then(function(response){
          vm.data.events.push(response);
        },function(error){
          console.log(error);
        });
    }

    function editEvent(events) {
      modalService.eventModal(events)
        .then(function(response){
          console.log(response);
          var index = vm.data.events.indexOf(events);
          vm.data.events[index] = response;
        },function(error){
          console.log(error);
        });
    }

    function deleteEvent(events) {
      swal({
        title: "¡Eliminar evento!",
        text: "¿Está seguro de querer eliminar el evento " + events.title + "?",
        type: "warning",
        confirmButtonText: "Si, Eliminar!",
        cancelButtonText: "No, Cancelar!",
        showCancelButton: true
      }, function (response) {
        if (response) {
          eventService.deleteEvent(events.id)
            .then(function (response) {
              console.log('here');
              _.remove(vm.data.events, function (n) { return n.id == response.data.id });
            }, function (error) {
              console.log('error', error);
            });
        }
      });
    }
  }
})();