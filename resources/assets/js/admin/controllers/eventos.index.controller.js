(function(){
  
  angular.module('sifme')
    .controller('eventosIndexController',eventosIndexController)
  
  eventosIndexController.$inject = [];
  
  function eventosIndexController(){
    var vm = this;
    
    
    activate();
    
    function activate(){
      console.log('Eventos index controller');
      
      // inicializacion de tabla
      $('#tabla_eventos').bootstrapTable();
    }
     
  }
})();