(function(){
  
  angular.module('sifme').controller('testController',testController);
  
  testController.$inject = ['$http','$linq'];
  
  function testController($http,$linq){
    var vm = this;
    vm.filesList = [];
    vm.submit = submitThings;
    activate();
    
    function activate(){
      console.log('hello');
      //Dropzone.instances[0].destroy();
      var opts = $.extend(true,{},opcionesDZ);
      opts.queuecomplete = updateFileLists;
      Dropzone.options.homeImgUpload = opts;
      updateFileLists();
    }
    
    function submitThings(){
      Dropzone.forElement("div#homeImgUpload").processQueue();
      updateFileLists();
    }
    
    function updateFileLists(){
      $http.get('/Admin/Archivos/Home/')
        .catch(function(error){console.log(error);})
        .then(function(response){
          if(response.status == 200){
            vm.filesList = $linq.Enumerable().From(response.data).Select(function(v){ return { url: '/images/home/' + v, name: v } }).ToArray();
          }else{
            console.log(response.status);
          }
        });
    }
  }
})();