(function(){
  
  angular.module('sifme')
    .controller('eventosEditController',eventosEditController)
    //.controller('eventosCreateController',eventosCreateController)
    .controller('calendarioController',calendarioController)
    .controller()
  
  eventosEditController.$inject = [];
  
  function eventosEditController(){
    var vm = this;
    
    activate();
    
    function activate(){
      $("#address").val(address);
      datepickers(moment(start_date),moment(end_date));
      initAutocomplete();
    }
  }
  
  function eventosCreateController(){
    var vm = this;
    
    activate();
    
    function activate(){
      datepickers();
    }
  }
  
  function eventosIndexController(){
    var vm = this;
    
    activate();
    
    function activate(){
      
    }
  }

   function calendarioController(){
    var vm = this;
    
    activate();
    

  }

  function datepickers(dtInit,dtEnd){

    $('#start_date').val('').datetimepicker({
                 locale: 'es',
                 defaultDate: dtInit ? dtInit : undefined
            });
    $("#end_date").val('').datetimepicker({
                locale: 'es',
                defaultDate: dtEnd ? dtEnd : undefined
            });
  }
})();