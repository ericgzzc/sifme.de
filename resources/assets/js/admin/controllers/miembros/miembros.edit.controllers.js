(function () {

  angular.module('sifme')
    .controller('miembrosEditController', miembrosEditController);

  miembrosEditController.$inject = ['user', 'permissionService', '$q', 'userService', '$uibModalInstance'];
  function miembrosEditController(user, permissionService, $q, userService, $uibModalInstance) {
    var vm = this;
    
    vm.form = {
      permissions: []
    };
    
    vm.actions = {
      close: close,
      save: save,
      deleteMe: deleteMe,
      submit: submit
    }
    
    vm.defaults = {
      title: 'Editar miembro:',
      subtitle: user.first_name + ' ' + user.last_name,
      validationOptions: {
        rules: {
          first_name: {
            required: true
          },
          last_name : {
            required: true
          },
          email : {
            required: true,
            email: true
          },
          email_confirm: {
            required: true,
            equalTo: '#email'
          }
          
        },
        messages: {
          first_name: {
            required: "Campo de Nombre(s) requerido"
          },
          last_name : {
            required: "Campo de Apellido(s) requerido"
          },
          email : {
            required: "Campo de Correo requerido",
            email: "El correo electronico debe tener formato de nombre@dominio.com"
          },
          email_confirm: {
            required: "Campo de Confirmacion de Correo requerido",
            equalTo: 'Los correos no coinciden, favor de verificar'
          }
        }
      }
    }

    activate();

    function activate() {
      var promises = [];

      promises.push(permissionService.getPermissionNames());
      promises.push(userService.getUserPermissions(user.id));

      $q.all(promises)
        .then(function (responses) {
          responses[0].data.forEach(function (permission) {
            var perm = _.find(responses[1].data, { id: permission.id })
            permission.value = perm ? true : false;
          });

          vm.form = {
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            email_confirm: user.email,
            permissions: responses[0].data
          }
        });
    }

    function save() {
      console.log(vm.form);
      userService.updateUser(vm.form,user.id)
        .then(function(response){
          user.first_name = vm.form.first_name;
          user.last_name = vm.form.last_name;
          user.email = vm.form.email;
          user.action = 2;
          $uibModalInstance.close(user);
        },function(error){
          $uibModalInstance.dismiss('Error al actualizar');
        });
    }

    function close() {
      $uibModalInstance.dismiss('close');
    }
    
    function deleteMe(){
      userService.deleteUser(user.id)
        .then(function(response){
          user.first_name = vm.form.first_name;
          user.last_name = vm.form.last_name;
          user.email = vm.form.email;
          user.action = 1;
          $uibModalInstance.close(user);
        },function(error){
          $uibModalInstance.dismiss('Error al eliminar');
        });
    }
    
    function submit(form){
      if(form.validate()){
        save();
      }
    }
  }

})();