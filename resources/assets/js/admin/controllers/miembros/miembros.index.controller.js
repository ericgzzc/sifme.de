(function () {
  'use strict';
  angular.module('sifme')
    .controller('miembrosIndexController', miembrosIndexController);

  miembrosIndexController.$inject = ['modalService', 'userService', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$uibModal'];

  // funcion de index de Miembros/ Usuarios
  function miembrosIndexController(modalService, userService, DTOptionsBuilder, DTColumnDefBuilder, $uibModal) {
    var vm = this;

    vm.options = {
      dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers'),
      dtColumnDefs: [
        DTColumnDefBuilder.newColumnDef(0),               // ID
        DTColumnDefBuilder.newColumnDef(1),               // First Name
        DTColumnDefBuilder.newColumnDef(2),               // Last Name
        DTColumnDefBuilder.newColumnDef(3)                // email
      ]
    };

    vm.actions = {
      addPerson: addPerson,
      editPerson: editPerson,
      deletePerson: deletePerson
    }

    vm.data = {
      users: []
    }

    activate();

    function activate() {
      userService.getUsers()
        .then(function (response) {
          vm.data.users = response.data;
        });
    }

    // modal or something
    function addPerson() {

      modalService.userModal()
        .then(function (result) {
          var action = result.action;
          delete result.action;
          switch (action) {
            case 0: // create
              vm.data.users.push(result);
              break;
          }
        }).catch(function (error) {
          console.log(error);
        });
    }

    function editPerson(user) {

      modalService.userModal(user)
        .then(function (result) {
          switch (result.action) {
            case 1://deleteMe
              _.remove(vm.data.users, function (n) { return n.id == result.id });
              break;
            case 2:// update
              result.action = undefined;
              var index = _.findIndex(vm.data.users, function (n) { return n.id == result.id });
              vm.data.users[index] = result;
              break;
          }
        }).catch(function (error) {
          console.log(error);
        })
    }

    function deletePerson(user) {
      swal({
        title: "¡Eliminar usuario!",
        text: "¿Está seguro de querer eliminar a " + user.first_name + " " + user.last_name + "?",
        type: "warning",
        confirmButtonText: "Si, Eliminar!",
        cancelButtonText: "No, Cancelar!",
        showCancelButton: true
      }, function (response) {
        if (response) {
          userService.deleteUser(user.id)
            .then(function (response) {
              console.log('here');
              _.remove(vm.data.users, function (n) { return n.id == response.data.id });
            }, function (error) {
              console.log('error', error);
            });
        }
      });

    }
  }

})();