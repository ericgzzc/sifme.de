(function () {

  angular.module('sifme')
    .controller('miembrosCreateController', miembrosCreateController);

  miembrosCreateController.$inject = ['permissionService', '$q', 'userService', '$uibModalInstance','toastr'];
  function miembrosCreateController(permissionService, $q, userService, $uibModalInstance, toastr) {
    var vm = this;
    
    vm.form = {
      permissions: [],
      first_name: "",
      last_name: "",
      email: ""
    }
    
    vm.actions = {
      close: close,
      save: save,
      submit: submit
    }
    
    vm.defaults = {
      title: 'Nuevo miembro:',
      subtitle: vm.form.first_name + ' ' + vm.form.last_name,
      validationOptions: {
        rules: {
          first_name: {
            required: true
          },
          last_name : {
            required: true
          },
          email : {
            required: true,
            email: true
          },
          email_confirm: {
            required: true,
            equalTo: '#email'
          }
          
        },
        messages: {
          first_name: {
            required: "Campo de Nombre(s) requerido"
          },
          last_name : {
            required: "Campo de Apellido(s) requerido"
          },
          email : {
            required: "Campo de Correo requerido",
            email: "El correo electronico debe tener formato de nombre@dominio.com"
          },
          email_confirm: {
            required: "Campo de Confirmacion de Correo requerido",
            equalTo: 'Los correos no coinciden, favor de verificar'
          }
        }
      }
    }
    
    activate();

    function activate() {
      var promises = [];

      permissionService.getPermissionNames()
        .then(function(response){
          response.data.forEach(function (permission) {
            permission.value =  false;
          });
          vm.form.permissions = response.data;
        },function(error){
          toastr.error('Error al cargarlo');
          $uibModalInstance.dismiss(error);
        });
      
      
    }
    
    function close() {
      $uibModalInstance.dismiss('close');
    }
    
    function save(){
      console.log('asd');
      userService.addUser(vm.form)
        .then(function(response){
          response.data.action = 0;
          $uibModalInstance.close(response.data);
        },function(error){
          switch(error.status){
            case 400: // bad request
              toastr.error('El correo ya se encuentra registrado','Error');
          }
          //$uibModalInstance.dismiss(error);
        });
    }
    
    function submit(form){
      if(form.validate()){
        save();
      }else{
        toastr.info('Favor de llenar los campos faltantes');
      }
    }
  }
  
})();