(function(){
  angular.module('sifme')
    .factory('permissionService',permissionService);
    
  permissionService.$inject = ['$http']
  
  function permissionService($http){
    return {
      getPermissionNames: getPermissionNames
    }
    
    
    function getPermissionNames(){
      return $http.get('/Recursos/Permisos');
    }
    
  }
})();