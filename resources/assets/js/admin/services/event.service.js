(function(){
  angular.module('sifme')
    .factory('eventService',eventService);
    
  eventService.$inject = ['$http','$q'];
  
  function eventService($http,$q){
    return {
      getEvent:getEvent,
      deleteEvent: deleteEvent,
      editEvent: editEvent,
      addEvent: addEvent
    }
    
    function getEvent(){
      return $http.get('/Recursos/Eventos');
    }
    
    function addEvent(Event){
      return $http.post('/Admin/Eventos/',Event);
    }
    
    function editEvent(Event,id){
      id = id ? id : Event.id;
      delete Event.id;
      return $http.patch('/Admin/Eventos/' + id, Event);
    }
    
    function deleteEvent(id){
      return $http.delete('/Admin/Eventos/' + id);
    }
  }
})();