(function(){
  angular.module('sifme')
    .factory('userService',userService);
    
  userService.$inject = ['$http','$q']
  
  function userService($http,$q){
    var service = {
      getUsers: getUsers,
      getUserPermissions: getUserPermissions,
      deleteUser: deleteUser,
      updateUser: updateUser,
      addUser: addUser
    }
    return service;
    
    function getComments(userId){
      var dfd = $q.defer();
      
      $http.get('/comentarios/show')
        .catch(function(error){
          dfd.reject(error);
        })
        .then(function(response){
          dfd.resolve(response);
        });
        
      
      return dfd.promise;
    }
    
    function getUsers(){
      return $http.get('/Recursos/Usuarios/');
    }
    
    function getUserPermissions(userId){
      return $http.get('/Recursos/Usuario/Permisos/' + userId);
    }
    
    function addUser(user){
      return $http.post('/Admin/Miembros/',user);
    }
    
    function deleteUser(id){
      return $http.delete('/Admin/Miembros/' + id);
    }
    
    function updateUser(user,id){
      id = user.id ? user.id : id;
      user.id = undefined;
      return $http.patch('/Admin/Miembros/' + id, user);
    }
    
  }
})();