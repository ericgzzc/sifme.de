(function(){
  angular.module('sifme')
    .factory('userService',userService);
    
  userService.$inject = ['$http','$q']
  
  function userService($http,$q){
    var service = {
      
    }
    return service;
    
    function getAllUsers(){
      return $http.get('')
    }
    
    function getComments(userId){
      var dfd = $q.defer();
      
      $http.get('/comentarios/show')
        .catch(function(error){
          dfd.reject(error);
        })
        .then(function(response){
          dfd.resolve(response);
        });
        
      
      return dfd.promise;
    }
  }
})();