(function(){
  angular.module('sifme')
    .factory('newsService',newsService);
    
  newsService.$inject = ['$http','$q'];
  
  function newsService($http,$q){
    return {
      getNews:getNews,
      deleteNews: deleteNews,
      editNews: editNews,
      addNews: addNews,
      getFollowers: getFollowers, 
      sendEmail: sendEmail,
      getFiles: getFiles
    }
    
    function getNews(){
      return $http.get('/Recursos/Noticias');
    }
    
    function addNews(news){
      return $http.post('/Recursos/Noticias/',news);
    }
    
    function editNews(news,id){
      id = id ? id : news.id;
      delete news.id;
      return $http.patch('/Recursos/Noticias/' + id, news);
    }
    
    function deleteNews(id){
      return $http.delete('/Recursos/Noticias/' + id);
    }
    
    function getFiles(id){
      return $http.get('/Recursos/Archivos/Noticia/' + id);
    }
    
    function getFollowers(eventoid){
      return $http.get('/Admin/Noticias/SeguidoresTodos/' + eventoid);
    }

    function sendEmail(data){
      return $http({
                method: 'POST',
                url: '/Admin/Noticias/enviarCorreo/'+eventoid,
                data: data
      });
    }
  }
})();