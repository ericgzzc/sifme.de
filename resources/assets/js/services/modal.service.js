(function(){
  'uses strict';
  
  angular.module('sifme')
    .factory('modalService',modalService);
  
  modalService.$inject = ['$uibModal'];  
  
  function modalService($uibModal){
    return {
      userModal: userModal,
      newsModal: newsModal,
      eventModal: eventModal
    }
    
    function newsModal(news){
      if(news){
        return $uibModal.open({
          controller: 'newsEditController',
          controllerAs: 'mvm',
          templateUrl: '/Modal/Noticia/Form/',
          windowClass: 'animated fadeIn in',
          size: 'lg',
          resolve: {
            news: function() { return news; }
          }
        }).result;
        
      }else{
        return $uibModal.open({
          controller: 'newsCreateController',
          controllerAs: 'mvm',
          templateUrl: '/Modal/Noticia/Form/',
          windowClass: 'animated fadeIn in',
          size: 'lg'
        }).result;
      }
    }
    
    function userModal(user){
      if(user){
        return $uibModal.open({
          controller: 'miembrosEditController',
          controllerAs: 'mb',
          templateUrl: '/Modal/Miembro/Form/',
          windowClass: 'animated fadeIn in',
          size: 'lg',
          resolve: {
            user: function(){ return user; }
          }
        }).result;
      }else{
        return $uibModal.open({
          controller: 'miembrosCreateController',
          controllerAs: 'mb',
          templateUrl: '/Modal/Miembro/Form/',
          windowClass: 'animated fadeIn in',
          size: 'lg'
        }).result;
      }
    }

    function eventModal(events){
      if(events){
        return $uibModal.open({
          controller: 'eventosEditController',
          controllerAs: 'mvm',
          templateUrl: '/Modal/Evento/Form/',
          windowClass: 'animated fadeIn in',
          size: 'lg',
          resolve: {
            events: function(){ return events; }
          }
        }).result;
      }else{
        return $uibModal.open({
          controller: 'eventosCreateController',
          controllerAs: 'mvm',
          templateUrl: '/Modal/Evento/Form/',
          windowClass: 'animated fadeIn in',
          size: 'lg'
        }).result;
      }
    }
  }
})();