@extends('layouts.admin.main')

@section('content')
<div class="container" ng-controller="informationController as vm">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Información General</div>
                <div class="panel-body">
                  {{ Form::model($info, array('method' => 'PUT', 'route' => array('admin.information.update', $info->id), 'files' => true, 'class' => 'form-horizontal', 'id' => 'theForm')) }}
                      <fieldset>
                        <div class="form-group">
                          <label for="inputWho" class="col-lg-2 control-label">¿Quiénes Somos?</label>
                          <div class="col-lg-10">
                            <textarea class="form-control textArea" rows="15" name="general_info" id="textWho">{{ $info->general_info }}</textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputMission" class="col-lg-2 control-label">Misión</label>
                          <div class="col-lg-10">
                            <textarea class="form-control textArea" rows="15" name="mission" id="textMission">{{ $info->mission }}</textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputVision" class="col-lg-2 control-label">Visión</label>
                          <div class="col-lg-10">
                            <textarea class="form-control textArea" rows="15" name="vision" id="textVision">{{ $info->vision }}</textarea>
                          </div>
                        </div>
                        <br>
                        
                        <br>
                        
                      </fieldset>
                  {!! Form::close() !!}
                  
                  <h5>Imágenes para la Página de Inicio: </h5>
                  <form action="" class="dropzone" dropzone="" id="dropzone" ng-url="vm.data.url" ng-token="'{{Session::token()}}'" ng-submit="vm.actions.upload">
                      <div class="dz-default dz-message">
                        Arrastre archivos aqui
                      </div>
                  </form>
                  <div class="form-group">
                    <div class="pull-right">
                      <button type="button" class="btn btn-primary" ng-click="vm.actions.saveFiles()">Guardar</button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection

@section('scripts')
<script type="text/javascript">
    Dropzone.autoDiscover = false;
</script>
@endsection