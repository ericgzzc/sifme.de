@extends('layouts.admin.main')
@section('content')


<div class="container" ng-controller="eventosController as vm">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
            <div class="panel-heading">Vista Previa del Evento </div>
            <div class="panel panel-user panel-default">
                <div class="panel-heading-user panel-heading title">{{ $evento->title }}
                </div>
                <div class="panel-body">
                    <div class="event">
                        <div style="margin-top:20px;margin-bottom:20px;">
                            <i class="glyphicon glyphicon-envelope a-user"></i>
                            <span class="material eventlabels" style="margin-left:10px;"> invited by</span>
                            <span class="author">
                                <a href="" class="author eventlabels">
                                    {{ $evento->author->first_name }} {{ $evento->author->last_name }}
                                </a>
                            </span>
                        </div>
                        <div style="margin-top:20px;margin-bottom:20px;">
                            <i class="glyphicon glyphicon-time"></i>
                            <span class="material eventlabels" style="margin-left:10px;"> from </span>
                            <a class="black eventlabels" href="">{{ date("d/m/Y g:H a", strtotime($evento->start_date)) }}</a>
                            <span class="material eventlabels"> to </span>
                            <a class="black eventlabels" href="">{{ date("d/m/Y g:H a", strtotime($evento->end_date)) }}</a>

                        </div>
                        <div style="margin-top:20px;margin-bottom:20px;">
                            <i class="glyphicon glyphicon-map-marker"></i>
                            <a class="black eventlabels" style="margin-left:10px;" href="">{{ $evento->address }}</a>
                        </div>
                    </div>

                    
                    <div class="contenidonoticia">
                        {{ $evento->content }}
                    </div>

                    <br>
                    <img class="img-responsive img-center images"src="/images/hoja.jpg">
                    <br>
                    <br>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>


  
@endsection
