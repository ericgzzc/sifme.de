@extends('layouts.admin.main')


@section('content')

<div class="container" ng-controller="eventosIndexController as vm">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Eventos</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary" ng-click="vm.actions.addEvent()">
                                <i class="fa fa-plus"> Agregar Evento</i>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <table
                                datatable="ng"
                                id="table"
                                dt-options="vm.options.dtOptions"
                                dt-column-defs="vm.options.dtColumnDefs"
                                class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                    <th>ID</th>
                                    <th>Título</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="event in vm.data.events track by event.id">
                                        <td ng-bind="event.id"></td>
                                        <td ng-bind="event.title"></td>
                                        <td ng-bind="event.start_date"></td>
                                        <td style="align:center;">
                                            <a class="btn btn-sm btn-primary" ng-click="vm.actions.editEvent(event)"><i class="fa fa-pencil"></i></a>
                                            <a style="display:none;" class="btn btn-sm btn-primary"href=""><i class="fa fa-list-ul"></i></a>
                                            <a style="display:none;" class="btn btn-sm btn-primary" href="" ><i class="fa fa-user"></i></a>
                                            <a class="btn btn-sm btn-danger" ng-click="vm.actions.deleteEvent(event)"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection