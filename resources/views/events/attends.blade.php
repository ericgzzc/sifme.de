@extends('layouts.admin.main')


@section('content')

<div class="container" ng-controller="eventosAsistentesController as vm">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Seguidores de Eventos</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <table
                                datatable="ng"
                                id="table"
                                dt-options="vm.options.dtOptions"
                                dt-column-defs="vm.options.dtColumnDefs"
                                class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Correo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="users in vm.data.users track by users.id">
                                        <td ng-bind="users.first_name"></td>
                                        <td ng-bind="users.last_name"></td>
                                        <td ng-bind="users.email"></td>
                                    </tr>
                                </tbody>
                            </table>

                            <form ng-submit="enviarCoreo()">
                            {{ Form::text('subject', null, ['class' => 'form-control box', 'placeholder'=> 'Escribe el Asunto', 'ng-model'=>'mensajeData.subject']) }}
                            {{ Form::textarea('mensaje', null, ['class' => 'form-control box', 'placeholder'=> 'Escribe tu mensaje', 'ng-model'=>'mensajeData.mensaje']) }}

        
                            <button class="btn btn-primary" id="comentar" ng-click="vm.submit()" type="submit">Enviar Correo</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection


@section("scripts")
    <script type="text/javascript">
        var eventoid =  {{ $id }};
    </script>
@endsection