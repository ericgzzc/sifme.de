@extends('layouts.admin.main')


@section('content')
<script type="text/javascript">
	var start_date = "{{$evento->start_date}}";
	var end_date = "{{$evento->end_date}}";
	var address = "{{$evento->address}}";
</script>
<div class="container" ng-controller="eventosEditController as vm">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Editar Evento</div>
                <div class="panel-body">
				    {{ Form::model($evento, array('method' => 'PATCH', 'route'=>['admin.events.update', $evento->id], 'files' => true)) }}
				    <div class="form-group">
				        {!! Form::label('title', 'Titulo:') !!}
				        {!! Form::text('title',null,['class'=>'form-control']) !!}
				    </div>
				    <div class="form-group">
				        {!! Form::label('content', 'Contenido:') !!}
				        {{ Form::textarea('content', null, ['size' => '30x5']) }}
				    </div>
				    <div class="form-group">
				        {!! Form::label('address', 'Direccion:') !!}
				        {!! Form::text('address',null,['class'=>'form-control']) !!}
				        <div id="map_event" class="map"></div>
				    </div>
				    <div class="form-group">
				         <img class="img-responsive img-center" src="/images/{{ $evento->imagen }}" alt="Imagen" > 
				    </div>
				    <div class="form-group">
				        {!! Form::label('imagen', 'Imagen:') !!}
				          {!! Form::file('imagen', null) !!}
				    </div>
				    <div class="form-group">
				        {!! Form::label('start_date', 'Fecha de Inicio:') !!}
				        {!! Form::text('start_date',null,['class'=>'form-control']) !!}
				    </div>
				    <div class="form-group">
				        {!! Form::label('end_date', 'Fecha de Fin:') !!}
				        {!! Form::text('end_date',null,['class'=>'form-control']) !!}
				    </div>
				    <div class="form-group">
				        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
				        {{ link_to_route('admin.events.show', 'Cancel', $evento->id, array('class' => 'btn')) }}
				    </div>
				    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection




