@extends('layouts.admin.main') @section('content')

<div class="container" ng-controller="eventosCreateController as vm">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-primary">
        <div class="panel-heading">Nuevo Evento</div>
        <div class="panel-body">                  

          {!! Form::open(['url' => 'Eventos', 'files'=>true] ) !!}
          <div class="form-group">
            {!! Form::label('title', 'Titulo:') !!} {!! Form::text('title',null,['class'=>'form-control']) !!}
          </div>
          <div class="form-group">
            {!! Form::label('content', 'Contenido:') !!} {{ Form::textarea('content', null, ['class' => 'form-control']) }}
          </div>
          <div class="form-group">
            {!! Form::label('address', 'Direccion:') !!} 
            {!! Form::text('address',null,['class'=>'form-control']) !!}
            <div id="map_event" class="map"></div>
          </div>
          <div class="form-group">
            {!! Form::label('image', 'Imagen:') !!} {!! Form::file('imagen') !!}
          </div>
          <div class="form-group">
            {!! Form::label('start_date', 'Fecha de Inicio:') !!} {!! Form::text('start_date',null,['class'=>'form-control']) !!}
          </div>
          <div class="form-group">
            {!! Form::label('end_date', 'Fecha de Fin:') !!} {!! Form::text('end_date',null,['class'=>'form-control']) !!}
          </div>
          <div class="form-group">
            {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
