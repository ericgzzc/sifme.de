@extends('layouts.admin.main')

@section('content')

<div class="container" ng-controller="miembrosCreateController as vm">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-primary">
        <div class="panel-heading">Agregar Nuevo Miembro</div>
        <div class="panel-body">
          {!! Form::open(['method' => 'post', 'route'=>['usuarios.store']]) !!}
          <div class="row">
            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon">Nombre(s)</span>
                <input type="text" class="form-control" placeholder="ej. Mario Antonio" name="first_name" required/> 
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon">Apellido(s)</span>
                <input type="text" class="form-control" placeholder="ej. Mario Antonio" name="last_name" required/> 
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon">Correo</span>
                <input type="text" class="form-control" placeholder="ej. Mario Antonio" name="email" required/> 
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon">Confirmar correo</span>
                <input type="text" class="form-control" placeholder="ej. Mario Antonio" name="email_confirm" required/> 
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <!-- TODO sacar nombres -->
              @foreach ($permisions as $index => $value)
                <div class="row col-md-12">
                  <label><input type="checkbox" name="{{$value}}" value="1">{{$value}}</label>
                </div>
              @endforeach
            </div>
          </div>
          <div class="row top-margin">
            <div class="col-md-12col-lg-offset2">
              {!! Form::button('Cancel', ['type' => 'reset', 'class' => 'btn btn-default']) !!}
              {!! Form::button('Save', ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection