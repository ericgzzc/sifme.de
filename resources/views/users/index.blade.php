@extends('layouts.admin.main')


@section('content')

<div class="container" ng-controller="miembrosIndexController as mb">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-primary">
        <div class="panel-heading">Miembros</div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-primary" ng-click="mb.actions.addPerson()">
                <i class="fa fa-user-plus"> Agregar Miembro</i>
              </button>  
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <table 
                datatable="ng"
                id="table"
                dt-options="mb.options.dtOptions"
                dt-column-defs="mb.options.dtColumnDefs"
                class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre(s)</th>
                    <th>Apellidos</th>
                    <th>Correo</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="user in mb.data.users track by user.id">
                    <td ng-bind="user.id"></td>
                    <td ng-bind="user.first_name"></td>
                    <td ng-bind="user.last_name"></td>
                    <td ng-bind="user.email"></td>
                    <td>
                      <a class="btn btn-sm btn-primary" ng-click="mb.actions.editPerson(user)"><i class="fa fa-pencil"></i></a>
                      <a class="btn btn-sm btn-danger" ng-click="mb.actions.deletePerson(user)"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
  <script>
    (function() {
     //$('#table').DataTable();
      //console.log('here');
    })();
  </script>
@endsection