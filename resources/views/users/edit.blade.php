@extends('layouts.admin.main') @section('content')

<div class="container" ng-controller="miembrosCreateController as vm">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-primary">
        <div class="panel-heading">Agregar Nuevo Miembro</div>
        <div class="panel-body">
          {!! Form::open(['method' => 'put', 'route'=>['usuarios.update',$user->id]]) !!}
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                  <label class="control-label">Nombre(s)</label>
                  <input type="text" class="form-control" placeholder="ej. Mario Antonio" name="first_name" value="{{$user->first_name}}" required/>
                </div>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                  <label class="control-label">Apellido(s)</label>
                  <input type="text" class="form-control" placeholder="ej. De La Rosa" name="last_name" value="{{$user->last_name}}" required/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                  <label class="control-label">Correo electronico</label>
                  <input type="text" class="form-control" placeholder="123@mail.com" name="email" value="{{$user->email}}" required/>
                </div>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                  <label class="control-label">Confirmar correo</label>
                  <input type="text" class="form-control" placeholder="123@mail.com" name="email_confirm" value="{{$user->email}}" required/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h5>Permisos</h5>
              </div>
              <div class="col-md-12">
                <!-- TODO hardcoded stuff -->
                @if(!is_null($user->permissions))
                <div class="row col-md-12">
                    <label><input type="checkbox" name="master" value="1" {{ $user->permissions->master ? 'checked' : ''}}>Administrador Maestro</label>
                </div>
                <div class="row col-md-12">
                    <label><input type="checkbox" name="post_events" value="1" {{$user->permissions->post_events? 'checked' : '' }}>Registrar Eventos</label>
                </div> 
                <div class="row col-md-12">
                    <label><input type="checkbox" name="post_news" value="1" {{$user->permissions->post_news? 'checked' : '' }}>Registrar Noticias</label>
                </div> 
                <div class="row col-md-12">
                    <label><input type="checkbox" name="add_users" value="1" {{$user->permissions->add_users? 'checked' : '' }}>Agregar Usuarios</label>
                </div> 
                <div class="row col-md-12">
                    <label><input type="checkbox" name="deactivate_users" value="1" {{$user->permissions->deactivate_users? 'checked' : '' }}>Desactivar Usuarios</label>
                </div> 
                <div class="row col-md-12">
                    <label><input type="checkbox" name="delete_users" value="1" {{$user->permissions->delete_users? 'checked' : '' }}>Eliminar Usuarios</label>
                </div>
                @else
                <div class="row col-md-12">
                    <label><input type="checkbox" name="master" value="1" >Administrador Maestro</label>
                </div>
                <div class="row col-md-12">
                    <label><input type="checkbox" name="post_events" value="1" >Registrar Eventos</label>
                </div> 
                <div class="row col-md-12">
                    <label><input type="checkbox" name="post_news" value="1" >Registrar Noticias</label>
                </div> 
                <div class="row col-md-12">
                    <label><input type="checkbox" name="add_users" value="1" >Agregar Usuarios</label>
                </div> 
                <div class="row col-md-12">
                    <label><input type="checkbox" name="deactivate_users" value="1" >Desactivar Usuarios</label>
                </div> 
                <div class="row col-md-12">
                    <label><input type="checkbox" name="delete_users" value="1" >Eliminar Usuarios</label>
                </div>
                @endif 
              </div>
            </div>
            <div class="row top-margin">
              <div class="col-md-12 col-lg-offset2">
                {!! Form::button('Cancel', ['type' => 'reset', 'class' => 'btn btn-default']) !!}
                {!! Form::button('Save', ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection