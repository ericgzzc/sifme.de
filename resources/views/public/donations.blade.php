@extends('layouts.public.main')

@section('content')
<div class="container" ng-controller="publicController as vm">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-user panel-default">
                <div class="panel-heading-user panel-heading title">
                </div>
                <div class="panel-body">
                    <h1 style="text-align:center;">¿Cómo Donar?</h1>
                    <br>
                    <br>
                    <div class="allmargins">
                        <div class="form-group">
                            <label class="material dlabels" style="margin-left:10px;font-weight:bold;">Banco: </label>
                            <label class="material dlabels">
                                {{ $info->bank_name }}
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="material dlabels" style="margin-left:10px;font-weight:bold;">Número de Cuenta: </label>
                            <label class="material dlabels">
                                {{ $info->bank_account }}
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="material dlabels" style="margin-left:10px;font-weight:bold;">CLABE: </label>
                            <label class="material dlabels">
                                {{ $info->clabe }}
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="material dlabels" style="margin-left:10px;font-weight:bold;">Nombre del Titular: </label>
                            <label class="material dlabels">
                                {{ $info->cardholders_name }}
                            </label>
                        </div>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
</div>
@endsection
