@extends('layouts.public.main')

@section('content')
<div class="container" ng-controller="noticiaCommentController as vm">
    <div class="row">
        <div class="col-md-12">

            @if(!Auth::guest())
            <a href="{{ route('member.news.home') }}" class="btn btn-primary" style="margin-bottom:20px;">Regresar a las Noticias</a>
            @else
            <a href="{{ route('public.news.home') }}" class="btn btn-primary" style="margin-bottom:20px;">Regresar a las Noticias</a>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-user panel-default">
                        <div class="panel-heading-user panel-heading title">{{ $noticia->title }}
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a class="black" ng-bind="noticia.sector"></a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <span>
                                                    <i class="glyphicon glyphicon-user a-user"></i>
                                                    <span class="material"> by</span>
                                                    <span class="author">
                                                        <a href="" class="author">
                                                            {{ $noticia->author->first_name }} {{ $noticia->author->last_name }}
                                                        </a>
                                                    </span>
                                                </span>
                                                <span>
                                                    <i class="glyphicon glyphicon-time"></i>
                                                    <a class="black" href="" ng-click="">{{ $noticia->created_at }}</a>
                                                </span>
                                            @if(!Auth::guest())
                                                <span ng-if="existe">
                                                    <i class="fa fa-star-o" id="iconoseguir"></i>
                                                    <a class="black" id="seguir" ng-click="followNew({{ $noticia->id }})" >Seguir Noticia</a>
                                                </span>
                                                <span ng-if="!existe">
                                                    <i class="fa fa-star" id="iconoseguir"></i>
                                                    <a class="black" id="seguir" ng-click="unfollowNew({{ $noticia->id }})" >No Seguir Noticia</a>
                                                </span>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <br>
                            @if(count($noticia->files) > 0 )
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="slider" class="flexslider" style="margin-left:auto;margin-right:auto;">
                                        <ul class="slides" >
                                            @foreach ($noticia->files as $file)
                                            <li><img src='{{$file->url}}' /></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endif        
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="contenidonoticia">
                                        {{ $noticia->content }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!Auth::guest())
            <div class="row">
                <div class="col-md-12">
                    <div class="control-label">
                        <h3> Comentarios (<span ng-bind="comments.length"></span>)</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div ng-repeat="comment in comments track by comment.id">
                        <div class="commentcontainer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="commenttop header pull-left">
                                        <span ng-bind="comment.first_name"></span> <span ng-bind="comment.last_name"></span>
                                    </div>
                                    <div class="commenttop delete pull-left" ng-if="{{Auth::user()->hasPermissionTo('de_admin')}} == 1 || {{Auth::user()->id }} == comment.user_id">
                                        <a href="#" ng-click="deleteComment(comment.id, comment.id_rel)" class="text-muted"><i class="fa fa-times" aria-hidden="true"></i> Eliminar comentario</a>
                                    </div>
                                    <div class="commenttop date pull-right">
                                        <span ng-bind="comment.created_at.date"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="commentcontent">
                                        <span ng-bind="comment.comment"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form ng-submit="submitComment()">
                        <div class="commentcontainer">
                            <div class="commentArea">
                                <textarea id="comment" ng-model="commentData.comment" class="form-control box"></textarea>
                            </div>
                            <div class="commenttop">
                                <button class="btn btn-primary" id="comentar" ng-click="vm.submit()" type="submit">Comentar</button>
                            </div>
                        </div>  
                    </form>
                </div>
            </div>
            <br>
            @endif
        </div>
    </div>
    <br>
    <br>
</div>
@endsection

@section("scripts")
    <script type="text/javascript">
        var noticia_id =  {{ $noticia->id }};
    </script>
@endsection