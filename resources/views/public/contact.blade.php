@extends('layouts.public.main')

@section('content')

<div class="container" ng-controller="enviarCorreoControler as vm">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-user">
                <div class="panel-body">
                    <div class="row">
                        <fieldset>
                            <div class="form-group" style="margin-left:15px;">
                                <h1> ¡Contáctanos! </h1>
                            </div>
                            <div class="col-md-6">
                                <div class="custom-container">
                                <form ng-submit="enviarCorreo()">
                                    <br>
                                    @if (Auth::user())
                                    <div class="form-group">
                                        <div>
                                            <input type="text" class="form-control" id="inputName" name="user_name" placeholder="Escribe tu Nombre" value="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}" ng-model="mensajeData.name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <input type="text" class="form-control" id="inputEmail" name="user_email" placeholder="Escribe tu Correo Electrónico" value="{{ Auth::user()->email }}" ng-model="mensajeData.email">
                                        </div>
                                    </div>
                                    @else
                                    <div class="form-group">
                                        <div>
                                            <input type="text" class="form-control" id="inputName" name="user_name" placeholder="Escribe tu Nombre" value="" ng-model="mensajeData.name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <input type="text" class="form-control" id="inputEmail" name="user_email" placeholder="Escribe tu Correo Electrónico" value="" ng-model="mensajeData.email">
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <div>
                                            <input type="text" class="form-control" id="inputSubject" name="email_subject" placeholder="Escribe el Asunto" value="" ng-model="mensajeData.asunto">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <textarea class="form-control textArea" rows="10" name="email_content" id="inputContent" placeholder="Escribe tu Mensaje" ng-model="mensajeData.mensaje"></textarea>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary">Enviar Correo</button>

                                     </form>
                                    <a type="button" class="btn btn-warning" id="verMapa" style="display: none;"></a>
                                    <input type="hidden" class="form-control" id="inputStreet" name="street" value="{{ $info->street }}" >
                                    <input type="hidden" class="form-control" id="inputNumber" name="street" value="{{ $info->number }}" >
                                    <input type="hidden" class="form-control" id="inputCity" name="street" value="{{ $info->city }}" >
                                    <input type="hidden" class="form-control" id="inputCountry" name="street" value="{{ $info->country }}" >
                                    <input type="hidden" class="form-control" id="inputContent" name="content" value="<h1>{{ $info->street}}</h1>" >
           
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="map-container" class="custom-container">
                                    <div id="map" class="map float-right"></div>
                                </div>
                            </div>
                            <br>
                        </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
