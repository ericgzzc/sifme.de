@extends('layouts.public.main')

@section('content')
<div class="container" ng-controller="noticiasController as vm">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            @if(!Auth::guest())
           <a href="{{ route('member.suscription.home') }}" class="btn btn-primary" style="margin-bottom:20px;">Ver Las Noticias Que Sigo</a>
            @endif

            <div ng-repeat="noticia in noticias">
            <div class="panel panel-user panel-default">

                <div class="panel-heading-user panel-heading title">
                    <a class="black" href='Noticia/<% noticia.id %>' ng-bind="noticia.title" ></a>
                </div>
                <div class="panel-body">
                    <div class="post">
                    <div>
                        <a class="black" ng-bind="noticia.sector" ></a>
                    </div>
                    <br>
                        <span>
                            <i class="glyphicon glyphicon-user a-user"></i>
                            <span class="material"> by</span>
                            <span class="author">
                            <a class="no-click" ng-bind="noticia.author_first_name">
                            </a>
                            </span>
                            <span class="material">
                            <a class="no-click" ng-bind="noticia.author_last_name"></a>
                            </span>
                        </span>

                        <span>
                        <i class="glyphicon glyphicon-time"></i>
                        <a class="black no-click" href="" ng-bind="noticia.created_at" ></a>
                        </span>
                       
                            
                            @if(!Auth::guest())
                            <span ng-show="<% noticia.seguir %>">
                            <i class="glyphicon glyphicon-star" id="iconoseguir"></i>
                            <a class="black" id="seguir" ng-click="unfollowNew(noticia.id)" >Seguir Noticia</a>
                            </span>
                            <span ng-hide="<% noticia.seguir %>">
                            <i class="glyphicon glyphicon-star-empty" id="iconoseguir"></i>
                            <a class="black" id="seguir" ng-click="followNew(noticia.id)" >No Seguir Noticia</a>
                            </span>
                            @endif

                    </div>    
                	<br>
                    <div id="slider" class="flexslider" ng-if="noticia.files.length > 0" style="margin-left:auto;margin-right:auto;">
                      <ul class="slides" >
                          <li ng-repeat="file in noticia.files"><img ng-src='<% file.url %>' alt='image' /></li>
                      </ul>
                    </div>

                    <br>
                    <br>
                    <div class="multiline-ellipsis contenidonoticia" ng-bind="noticia.content">

                    </div >
                    <hr class="hr-user">
                    <div class="col-sm-1 nopadding read">
                        <a type="button" class="a-user read-more " href='Noticia/<% noticia.id %>'>Leer Más </a>
                        <br>
                    </div>
                </div>
            </div>
            </div>
            

        </div>
    </div>
</div>
@endsection
