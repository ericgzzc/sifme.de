@extends('layouts.public.main')

@section('content')
<div class="container" ng-controller="">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            @if(!Auth::guest())
            <a href="{{ route('member.events.calendar') }}" class="btn btn-primary" style="margin-bottom:20px;">Regresar al Calendario</a>
            @endif

            <div class="panel panel-user panel-default">

                <div class="panel-heading-user panel-heading title">
                    <a class="black" >Eventos a los que Asistiré</a>
                </div>
                <span>
                <div class="panel-body">

                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Título</th>
                          <th>Fecha</th>
                        </tr>
                      </thead>
                        <tbody>
                        @foreach($eventos as $evento)
                        <tr>
                          <td><a href='/Miembros/Evento/{{ $evento->id }}'>{{ $evento->id }}</a></td>
                          <td><a href='/Miembros/Evento/{{ $evento->id }}'>{{ $evento->title }}</a></td>
                          <td><a href='/Miembros/Evento/{{ $evento->id }}'>{{ $evento->start_date }}</a></td>
                        </tr>
                        @endforeach
                    </tbody>

                    </table> 
                </span>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
