@extends('layouts.public.main')

@section('content')
<div class="container" ng-controller="noticiasController as vm">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            @if(!Auth::guest())
            <a href="{{ route('member.news.home') }}" class="btn btn-primary" style="margin-bottom:20px;">Regresar a las Noticias</a>
            @endif

            <div class="panel panel-user panel-default">

                <div class="panel-heading-user panel-heading title">
                    <a class="black" >Las Noticias Que Sigo</a>
                </div>
                <span>
                <div class="panel-body">

                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Título</th>
                          <th>Fecha de Publicación</th>
                        </tr>
                      </thead>
                        <tbody>
                        @foreach($noticias as $noticia)
                        <tr>
                          <td><a href='/Miembros/Noticia/{{ $noticia->id }}'>{{ $noticia->id }}</a></td>
                          <td><a href='/Miembros/Noticia/{{ $noticia->id }}'>{{ $noticia->title }}</a></td>
                          <td><a href='/Miembros/Noticia/{{ $noticia->id }}'>{{ $noticia->created_at }}</a></td>
                        </tr>
                        @endforeach
                    </tbody>

                    </table> 
                </span>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
