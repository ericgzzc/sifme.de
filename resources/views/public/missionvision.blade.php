@extends('layouts.public.main')

@section('content')
<div class="container" ng-controller="publicController as vm">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-user panel-default">
                <div class="panel-heading-user panel-heading title">
                </div>
                <div class="panel-body">
                    <h1 style="text-align:center;">Nuestra Misión</h1>
                    <br>
                    <br>
                    <p class="eventlabels allmargin" style="text-align: justify;">
                        {{ $info->mission }}
                    </p>
                    <h1 style="text-align:center;">Nuestra Visión</h1>
                    <br>
                    <br>
                    <p class="eventlabels allmargin" style="text-align: justify;">
                        {{ $info->vision }}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
</div>
@endsection
