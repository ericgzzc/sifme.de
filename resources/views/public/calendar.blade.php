@extends('layouts.public.main')

@section('content')

<div class="container" >
	<div class="row">
        <div class="span6">
        	@if(!Auth::guest())
           <a href="{{ route('member.asists.home') }}" class="btn btn-primary" style="margin-bottom:20px;">Ver los Eventos que Asisto</a>
            @endif

         {!! $calendar->calendar() !!}
   		</div>
	</div>
</div>
@endsection

@section('scripts')
	<!-- mjnhbgv -->
	 {!! $calendar->script() !!}
	<!-- mjnhbgv -->
@overwrite