@extends('layouts.public.main')

@section('content')
<div class="container" ng-controller="eventoCommentController as vm">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if(!Auth::guest())
           <a href="{{ route('member.events.calendar') }}" class="btn btn-primary" style="margin-bottom:20px;">Regresar al Calendario</a>
            @else
            <a href="{{ route('public.calendar.home') }}" class="btn btn-primary" style="margin-bottom:20px;">Regresar al Calendario</a>
            @endif

            <div class="panel panel-user panel-default">
                <div class="panel-heading-user panel-heading title">{{ $evento->title }}
                </div>
                <div class="panel-body">
                    <div class="event">
                        <div style="margin-top:20px;margin-bottom:20px;">
                            <i class="glyphicon glyphicon-envelope a-user"></i>
                            <span class="material eventlabels" style="margin-left:10px;"> invited by</span>
                            <span class="author">
                                <a href="" class="author eventlabels">
                                    {{ $evento->author->first_name }} {{ $evento->author->last_name }}
                                </a>
                            </span>
                        </div>
                        <div style="margin-top:20px;margin-bottom:20px;">
                            <i class="glyphicon glyphicon-time"></i>
                            <span class="material eventlabels" style="margin-left:10px;"> from </span>
                            <a class="black eventlabels" href="">{{ date("d/m/Y g:H a", strtotime($evento->start_date)) }}</a>
                            <span class="material eventlabels"> to </span>
                            <a class="black eventlabels" href="">{{ date("d/m/Y g:H a", strtotime($evento->end_date)) }}</a>

                        </div>
                        <div style="margin-top:20px;margin-bottom:20px;">
                            <i class="glyphicon glyphicon-map-marker"></i>
                            <a class="black eventlabels" style="margin-left:10px;" href="">{{ $evento->address }}</a>
                        </div>

                            @if(!Auth::guest())
                            <button id="asistir_si" ng-show="existe" type="button" class="btn btn-primary btn-lg " ng-click="asistir({{ $evento->id }})">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Asistir
                            </button>
                       
                            <button id="asistir_no" ng-hide="existe"  type="button" class="btn btn-success btn-lg " ng-click="noasistir({{ $evento->id }})">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> No Asistir
                            </button>
                            @endif
                       
                    </div>

                    
                    <div class="contenidonoticia">
                        <p>
                             {{ $evento->content }}
                        </p>
                        
                    </div>

                	<br>
                    <div class="contenidonoticia">
                    </div>
                    <br>
                    <br>
                </div>
            </div>

            @if(!Auth::guest())
            <div class="control-label">
            @if(!is_null($evento->comments))
            	<h3> Comentarios (<span ng-bind="comments.length"></span>)</h3>
            <div ng-repeat="comment in comments">
            <div class="commentcontainer">    
                <div class="commenttop header">
                    <span ng-bind="comment.first_name"> 

                    </span>
                    <span ng-bind="comment.last_name"> 

                    </span>

                </div>
                <div class="commenttop date">
                    <span ng-bind="comment.created_at.date"> 

                    </span>
                </div>
                <div class="commenttop delete" ng-if="comment.user_id == {{ Auth::user()->id }} ">
                 
                        <a href="#" ng-click="deleteComment(comment.id, comment.id_rel)" class="text-muted">Delete</a>
                
                </div>
                <div class="commentcontent">
                    <span ng-bind="comment.comment"> 

                    </span>
                </div>
            </div>
            </div>
            @else
                <h3> Comentarios (0)</h3>
            @endif
            <form ng-submit="submitComment()">
           
            <div class="commentcontainer">    
                <div class="commentArea">
                 {{ Form::textarea('comment', null, ['class' => 'form-control box', 'ng-model'=>'commentData.comment']) }}
                </div>
                <div class="commenttop">
                 {!! Form::submit('Comentar', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
             </form>

            <br>
            <br>
        </div>
        @endif
    </div>
    <br>
    <br>
</div>
@endsection

@section("scripts")
    <script type="text/javascript">
        var evento_id =  {{ $evento->id }};
    </script>
@endsection