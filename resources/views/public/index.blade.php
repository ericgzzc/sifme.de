@extends('layouts.public.main')

@section('content')

<div class="container" ng-controller="publicController as vm">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div id="slider" class="flexslider">
              <ul class="slides" >
                @foreach ($files as $file)
                  <li><img src='/images/home/{{$file}}' /></li>
                @endforeach
              </ul>
            </div>
        </div>
        <h5 class="mensaje" style="text-align:center;">Yo te desposaré conmigo para siempre; te desposaré conmigo en justicia y en derecho en amor y en compasión, te desposaré conmigo en fidelidad, y tú conocerás a Yahveh.</h5>
            <h5 class="mensaje" style="text-align:right;">Oseas 2, 21-22</h5>
    </div>
</div>
          
          
@endsection
