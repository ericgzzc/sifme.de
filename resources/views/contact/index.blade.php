@extends('layouts.admin.main')

@section('content')

<div class="container" ng-controller="contactController as vm">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Información de Contacto</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::model($contact, array('method' => 'PUT', 'route' => array('admin.contact.update', $contact->id), 'files' => false, 'class' => 'form-horizontal')) }}
                                    <fieldset>
                                        <div class="custom-container">
                                            <div class="form-group">
                                                <label for="inputName" class="col-lg-2 control-label">Empresa</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="inputName" name="company_name" placeholder="Nombre de la Empresa" value="{{ $contact->company_name }}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail" class="col-lg-2 control-label">Correo Electrónico</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="inputEmail" name="contact_email" placeholder="Correo Electrónico" value="{{ $contact->contact_email }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputPhone" class="col-lg-2 control-label">Teléfono</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="inputPhone" name="contact_phone" placeholder="Teléfono" value="{{ $contact->contact_phone }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputStreet" class="col-lg-2 control-label">Calle</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="inputStreet" placeholder="Calle" name="street" value="{{ $contact->street }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputNumber" class="col-lg-2 control-label">Número</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="inputNumber" placeholder="Número" name="number" value="{{ $contact->number }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputCity" class="col-lg-2 control-label">Ciudad</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="inputCity" placeholder="Ciudad" name="city" value="{{ $contact->city }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputState" class="col-lg-2 control-label">Estado</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="inputState" placeholder="Estado" name="state" value="{{ $contact->state }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputCountry" class="col-lg-2 control-label">País</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="inputCountry" placeholder="País" name="country" value="{{ $contact->country }}">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <div class="col-lg-10 col-lg-offset-4">
                                                <a type="button" class="btn btn-warning" id="verMapa">
                                                    Ver en Mapa
                                                </a>
                                                </div>
                                            </div>
                                            </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="map-container" class="custom-container">
                                                    <div id="map" class="map float-right"></div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                              <div class="col-lg-10 col-lg-offset-2">
                                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                              </div>
                                            </div>
                                    </fieldset>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
