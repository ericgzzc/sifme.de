@extends('layouts.admin.main')
@section('content')


<div class="container" ng-controller="eventosController as vm">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">{{ $evento->title }}</div>
                    <div class="panel-body">
                    <div class="row-centered">
                        <img class="img-responsive img-center" src="/images/{{ $evento->imagen }}" alt="Imagen" width="851" height="315"> 
                     </div>
                        <div class="row">
                            <div class="col-md-6">
                                <form class="form-horizontal">
                                    <fieldset>
                                        <div class="custom-container">
                                            {{ $evento->content }}
                                        </div>
                                         </div>
                                            <div class="col-md-6">
                                                <div id="map-container" class="custom-container">
                                                    <div id="map"></div>
                                                </div>
                                            </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


  
@endsection
