@extends('layouts.admin.main')


@section('content')

<div class="container" ng-controller="eventosIndexController as vm">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Evento </div>
                <div class="panel-body">
					<a href="{{url('/Eventos/create')}}" class="btn btn-success">Agrega Evento</a>
          
          <table id="tabla_eventos"
                data-search="true"
                data-show-columns="true"
                data-show-export="true"
                data-pagination="true"
                data-filter-control="true">
                <thead>
                  <tr>
                      <th rowspan="2" data-field="title" data-sortable="true">Titulo</th>
                      <th rowspan="2" data-field="address" data-sortable="true">Direccion</th>
                      <th colspan="2">Acciones</th>
                  </tr>
                  <tr>
                    
                  </tr>
                </thead>
                <tbody>
                @foreach ($eventos as $evento)
                  <tr>
                      <td>{{ $evento->title }}</td>
                      <td>{{ $evento->address }}</td>           
                      <td><a href="{{route('Eventos.show',$evento->id)}}" class="btn btn-primary">Read</a></td>
                      <td><a href="{{route('Eventos.edit',$evento->id)}}" class="btn btn-warning">Update</a></td>
                      <td>
                       {!! Form::open(['method' => 'DELETE', 'route'=>['Eventos.destroy', $evento->id]]) !!}
                       {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                       {!! Form::close() !!}
                     </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
                
    
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection




