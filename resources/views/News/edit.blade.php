@extends('layouts.admin.main')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Crear Noticia</div>
                <div class="panel-body">
                	{{ Form::model($noticia, array('method' => 'PUT', 'route' => array('noticias.update', $noticia->id), 'files' => true, 'class' => 'form-horizontal')) }}
						<div class="form-group">
					      <label for="inputTitle" class="col-lg-2 control-label">Título</label>
					      <div class="col-lg-10">
					        <input type="text" class="form-control" name="title" id="inputTitle" placeholder="Título" value="{{ $noticia->title }}" >
					      </div>
					    </div>
					    <div class="form-group">
					      <label for="inputDate" class="col-lg-2 control-label">Fecha de Publicación</label>
					      <div class="col-lg-10">
					        <input type="text" class="form-control" name="created_at" id="inputDate" placeholder="Fecha de Publicación" value="{{ date('m/d/Y G:H', strtotime($noticia->created_at)) }}" disabled>
					      </div>
					    </div>
					    
					    <div class="form-group">
					      <label for="textContent" class="col-lg-2 control-label">Contenido</label>
					      <div class="col-lg-10">
					        <textarea class="form-control textArea" name="content" rows="15" id="textContent">{{ $noticia->content }}</textarea>
					      </div>
					    </div>

					    <br>
							<div action="/upload" class="dropzone needsclick dz-clickable" id="demo-upload">
							  <div class="dz-message needsclick">
							    Arrastra aquí los archivos que se deseen adjuntar.<br>
							  </div>
							</div>
					    <br>
					    <div class="form-group">
                          <div class="col-lg-10 col-lg-offset-2">
					        {{ link_to_route('noticias.show', 'Cancel', $noticia->id, array('class' => 'btn btn-default')) }}
					        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
					       </div>
					    </div>
				    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection