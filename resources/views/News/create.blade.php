@extends('layouts.admin.main') @section('content')

<div class="container" ng-controller="noticiasUpdateController as vm">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-primary">
        <div class="panel-heading">Crear Noticia</div>
        <div class="panel-body">
          {!! Form::open(['url' => 'Noticias', 'files'=>true, 'class' => 'form-horizontal'] ) !!}
            <div class="form-group">
              <label for="inputTitle" class="col-lg-2 control-label">Título</label>
              <div class="col-lg-10">
                <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Título">
              </div>
            </div>

            <div class="form-group">
              <label for="textDescription" class="col-lg-2 control-label">Descripción</label>
              <div class="col-lg-10">
                <textarea class="form-control textArea" rows="15" name="content" id="textDescription"></textarea>
              </div>
            </div>
            <br>
            <div action="/upload" class="dropzone needsclick dz-clickable" id="demo-upload">
              <div class="dz-message needsclick">
                Arrastra aquí los archivos que se deseen adjuntar.
                <br>
              </div>
            </div>
            <br>
            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                {!! Form::button('Cancel', ['type' => 'reset', 'class' => 'btn btn-default']) !!}
                {!! Form::button('Save', ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
              </div>
              
              <div class="form-group">
                
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection