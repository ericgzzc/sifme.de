@extends('layouts.admin.main')


@section('content')

<div class="container" ng-controller="noticiasIndexController as vm">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Noticias</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary" ng-click="vm.actions.addNews()">
                                <i class="fa fa-plus"> Agregar Noticia</i>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <table
                                datatable="ng"
                                id="table"
                                dt-options="vm.options.dtOptions"
                                dt-column-defs="vm.options.dtColumnDefs"
                                class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                    <th>ID</th>
                                    <th>Título</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="news in vm.data.news track by news.id">
                                        <td ng-bind="news.id"></td>
                                        <td ng-bind="news.title"></td>
                                        <td ng-bind="news.created_at"></td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" ng-click="vm.actions.editNews(news)"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-sm btn-danger" ng-click="vm.actions.deleteNews(news)"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection