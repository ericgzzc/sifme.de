@extends('layouts.admin.main')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <br>
           <a href="{{ url('/Noticias') }}" class="btn btn-primary" style="margin-bottom:20px;">Regresar a Noticias</a>
           <br>
            <div class="panel panel-primary">
            <div class="panel-heading">Vista Previa de la Noticia </div>
            <div class="panel panel-user panel-default">
                <div class="panel-heading-user panel-heading title">{{ $noticia->title }}
                </div>
                <div class="panel-body">
                    <div class="post">
                        <span>
                            <i class="glyphicon glyphicon-user a-user"></i>
                            <span class="material"> by</span>
                            <span class="author">
                            <a href="" class="author">
                                {{ $noticia->author->first_name }} {{ $noticia->author->last_name }}
                            </a>
                            </span>
                        </span>

                        <span>
                        <i class="glyphicon glyphicon-time"></i>
                        <a class="black" href="">{{ $noticia->created_at }}</a>
                        </span>

                        <span>
                        <i class="fa fa-star-o" id="iconoseguir"></i>
                        <a class="black" id="seguir">Seguir Noticia</a>
                        </span>

                    </div>    
                  <br>
                    <img class="img-responsive img-center images"src="/images/hoja.jpg">
                    <br>
                    <br>
                    <div class="contenidonoticia">
                        {{ $noticia->content }}
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>
</div>
  
@endsection