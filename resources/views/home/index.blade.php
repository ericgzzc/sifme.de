@extends('layouts.admin.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <img class="img-responsive img-center images"src="/images/logo_sifme.png" style="margin-left:auto;margin-right:auto;">
            <h3 class="mensaje" style="text-align:center;">Yo te desposaré conmigo para siempre; te desposaré conmigo en justicia y en derecho en amor y en compasión, te desposaré conmigo en fidelidad, y tú conocerás a Yahveh.</h3>
            <br>
            <h4 class="mensaje" style="text-align:right">Oseas 2, 21-22</h4>
    </div>
    
    
</div>
@endsection
