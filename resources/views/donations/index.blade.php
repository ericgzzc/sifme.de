@extends('layouts.admin.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Información para Donar</div>
                <div class="panel-body">
                  {{ Form::model($donations, array('method' => 'PUT', 'route' => array('admin.donations.update', $donations->id), 'files' => false, 'class' => 'form-horizontal')) }}
                      <fieldset>
                        <div class="form-group">
                          <label for="inputBank" class="col-lg-2 control-label">Nombre del Banco</label>
                          <div class="col-lg-10">
                            <input type="text" class="form-control" id="inputBank" name="bank_name" placeholder="Nombre del Banco" value="{{ $donations->bank_name }}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputAccount" class="col-lg-2 control-label">Número de Cuenta</label>
                          <div class="col-lg-10">
                            <input type="text" class="form-control" id="inputAccount" name="bank_account" placeholder="Número de Cuenta" value="{{ $donations->bank_account }}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputCLABE" class="col-lg-2 control-label">CLABE</label>
                          <div class="col-lg-10">
                            <input type="text" class="form-control" id="inputCLABE" name="clabe" placeholder="CLABE" value="{{ $donations->clabe }}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputName" class="col-lg-2 control-label">Nombre del Titular</label>
                          <div class="col-lg-10">
                            <input type="text" class="form-control" id="inputName" name="cardholders_name" placeholder="Nombre del Titular" value="{{ $donations->cardholders_name }}">
                          </div>
                        </div>

                        <br>
                        <div class="form-group">
                          <div class="col-lg-10 col-lg-offset-2">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                          </div>
                        </div>
                      </fieldset>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
  
@endsection
