<div class="modal-header">
  <h4><%mvm.defaults.title%></h4>
  <h5 ng-bind="mvm.defaults.subtitle"></h5>
</div>
<div class="modal-body">
  <form name="newsForm" action="#" ng-validate="mvm.defaults.validationOption">
    <h5>Datos de noticia</h5>
    <div class="row">
      <div class="col-md-10">
        <label for="news_title">Titulo</label>
        <input id="news_title" name="title" ng-model="mvm.form.title" class="form-control"/>
      </div>
      <div class="col-md-2">
        <label for="news_public">Publico</label>
        <input
          name="public"
          bs-switch
          ng-model="mvm.form.public"
          type="checkbox"
          switch-size="<% 'normal' %>"
          switch-animate="<% true %>"
          switch-on-text="<% 'Sí' %>"
          switch-off-text="<% 'No' %>"
          switch-readonly="<% false %>"/> 
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label for="news_sector">Sector</label>
        <input id="news_sector" name="sector" class="form-control" ng-model="mvm.form.sector" />
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-12">
        <label for="news_content">Contenido</label>
        <textarea name="content" class="form-control" ng-model="mvm.form.content"></textarea>
      </div>
    </div>
    
  </form>
  <br />
  <div class="row scrollable" ng-if="mvm.form.files">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Preview</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbdoy>
        <tr ng-repeat="file in mvm.form.files">
          <td class="thumbnail">
            <img ng-src="<%file.url%>" alt="Image" />
          </td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
  <h5>Archivos adjuntos</h5>
  <form action="" class="dropzone" dropzone="" id="dropzone" ng-url="mvm.defaults.url" ng-token="'{{Session::token()}}'" ng-submit="mvm.actions.upload">
      <div class="dz-default dz-message">
        Arrastre archivos aqui
      </div>
  </form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-danger pull-left" ng-click="mvm.actions.deleteMe()" data-dismiss="modal" ng-if="mvm.actions.deleteMe">Eliminar</button>
  <button type="button" class="btn btn-success" data-dismiss="modal" ng-click="mvm.actions.submit(newsForm)">Guardar</button>
  <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="mvm.actions.close()">Close</button>
</div>