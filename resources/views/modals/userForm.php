<div class="modal-header">
  <h4><%mb.defaults.title%></h4>
  <h5 ng-bind="mb.defaults.subtitle"></h5>
</div>
<div class="modal-body">
  <form name="memberForm" action="#" ng-validate="mb.defaults.validationOptions">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label">Nombre(s)</label>
          <input type="text" class="form-control" placeholder="ej. Mario Antonio" name="first_name" ng-model="mb.form.first_name" required/>
        </div>
      </div>
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label">Apellido(s)</label>
          <input type="text" class="form-control" placeholder="ej. De La Rosa" name="last_name" ng-model="mb.form.last_name" required/>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label">Correo electronico</label>
          <input type="text" class="form-control" placeholder="nombre@dominio.com" id="email" name="email" ng-model="mb.form.email" required/>
        </div>
      </div>
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label">Confirmar correo</label>
          <input type="text" class="form-control" placeholder="nombre@dominio.com" name="email_confirm" ng-model="mb.form.email_confirm" required/>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Permisos</h5>
        </div>
        <table class="table table-striped">
          <tbody>
          <tr ng-repeat="permission in mb.form.permissions">
            <td>
              <%permission.description%>
              <div class="pull-right">
                <input
                  bs-switch
                  ng-model="permission.value"
                  type="checkbox"
                  switch-size="<% 'mini' %>"
                  switch-animate="<% true %>"
                  switch-on-text="<% 'Sí' %>"
                  switch-off-text="<% 'No' %>"
                  switch-readonly="<% false %>">
              </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-danger pull-left" ng-click="mb.actions.deleteMe()" data-dismiss="modal" ng-if="mb.actions.deleteMe">Eliminar</button>
  <button type="button" class="btn btn-success" data-dismiss="modal" ng-click="mb.actions.submit(memberForm)">Guardar</button>
  <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="mb.actions.close()">Close</button>
</div>