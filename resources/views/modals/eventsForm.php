<div class="modal-header">
  <h4><%mvm.defaults.title%></h4>
  <h5 ng-bind="mvm.defaults.subtitle"></h5>
</div>
<div class="modal-body">
  <form name="eventForm" action="#" ng-validate="mvm.defaults.validationOptions">
    <div class="row">
      <div class="col-md-10 col-sm-10">
        <div class="form-group">
          <label class="control-label">Título</label>
          <input type="text" class="form-control" placeholder="Título del evento" name="title" ng-model="mvm.form.title" required/>
        </div>
      </div>
      <div class="col-md-2">
        <label for="events_public">Publico</label>
        <input
          id="events_public"
          name="public"
          bs-switch
          ng-model="mvm.form.public"
          type="checkbox"
          switch-size="<% 'normal' %>"
          switch-animate="<% true %>"
          switch-on-text="<% 'Sí' %>"
          switch-off-text="<% 'No' %>"
          switch-readonly="<% false %>"/> 
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label">Descripción</label>
          <input type="text" class="form-control" placeholder="Descripción del evento" name="content" ng-model="mvm.form.content" required/>
        </div>
      </div>
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label">Dirección</label>
          <input type="text" class="form-control" placeholder="Calle Lala 123" name="address" ng-model="mvm.form.address" required/>
        </div>
      </div>
      <div class="col-md-12 col-sm-12">
        <div class="form-group">
          <label class="control-label">Fecha de inicio</label>
          <input id="start_date" type="text" class="form-control" placeholder="dd/mm/aaaa" name="start_date" ng-model="mvm.form.start_date" required/>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label class="control-label">Fecha de fin</label>
          <input id="end_date" type="text" class="form-control" placeholder="dd/mm/aaaa" name="end_date" ng-model="mvm.form.end_date" required/>
        </div>
      </div>
    </div>
  </form>
 
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-danger pull-left" ng-click="mb.actions.deleteMe()" data-dismiss="modal" ng-if="mvm.actions.deleteMe">Eliminar</button>
  <button type="button" class="btn btn-success" data-dismiss="modal" ng-click="mvm.actions.submit(eventForm)">Guardar</button>
  <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="mvm.actions.close()">Close</button>
</div>