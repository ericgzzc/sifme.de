var gulp = require('gulp');
var elixir = require('laravel-elixir');
var bower = require('gulp-bower');
var conf = require('./config')
var wiredep = require('wiredep');
var del = require('del');
var task = elixir.Task;
var vinylPaths = require('vinyl-paths');
var $ = require('gulp-load-plugins')();
require('gulp-concat');


elixir.extend('remove', function(path) {
    new task('remove', function() {
        return del(path);
    });
});

elixir.extend('bower',function(){
    new task('bower',function(){
        return bower();
    });
});

elixir.extend('buildLayouts',function(){
    new task('buildLayouts',function(){
         return gulp.src('tempViews/**/*.php')
                .pipe(gulp.dest('resources/views/'));
    })
    .watch('tempLayouts/**/*.php');
});

elixir.extend('injection',function(source,target,dest){
    new task('injection',function(){
         return gulp.src(target)
            .pipe($.inject(gulp.src(source),{ignorePath: 'public/'}))
            .pipe(gulp.dest(dest));
    })
    .watch('tempLayouts/**/*.php');
});

elixir.extend('buildFonts',function(){
    new task('buildFonts',function(){
    
        return gulp.src([
                'resources/assets/**/*.{eot,svg,ttf,woff,woff2}',
                'public/bower_components/**/*.{eot,svg,ttf,woff,woff2}'
            ])
            .pipe($.flatten())
            .pipe(gulp.dest('public/fonts'))
            .pipe(gulp.dest('public/css/fonts'));
    });
});

elixir.extend('clean',function(dirs){
   new task('clean',function(){
      return gulp.src([
            'public/css/',
            'public/js/',
            'public/images',
            'public/img',
            'public/lang',
            'public/fonts',
            'resources/views/layouts'
        ])
        .pipe(vinylPaths(del)); 
   });
});

elixir.extend('images',function(){
   new task('images',function(){
      return gulp.src([
            'resources/assets/images/**/*.{png,jpg,lang}'            
        ])
        .pipe(gulp.dest('public/images'))
        .pipe(gulp.src('public/bower_components/**/*.{png,jpg,lang}'))
        .pipe($.flatten())
        .pipe(gulp.dest('public/images'));
   })
   .watch('resources/assets/images');
});