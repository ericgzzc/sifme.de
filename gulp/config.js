exports.wiredep = function () {
    return {
        wireConfig: {},
        wireOpts: {
            fileTypes: {
                php: {
                    block: /(([ \t]*)<!--\s*bower:*(\S*)\s*-->)(\n|\r|.)*?(<!--\s*endbower\s*-->)/gi,
                    detect: {
                        js: /<script.*src=['"]([^'"]+)/gi,
                        css: /<link.*href=['"]([^'"]+)/gi
                    },
                    replace: {
                        js: '<script src="{{ URL::asset(\'{{filePath}}\') }}"></script>',
                        css: '<link rel="stylesheet" href="{{ URL::asset(\'{{filePath}}\') }}" />'
                    },

                }
            },
            exclude: ['/bower_components/bootstrap-material-design/dist/css/']
        }
    }
}