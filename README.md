## Prerequisitos
* npm
* composer
* wamp o PHP y MySQL

# Setup / Update
#### Correr los siguientes comandos
* Composer Install
* Composer Update
* npm install
* bower install
* gulp

#### Correr servidor local
* gulp watch
* php artisan serve

# Base de datos

Se debe crear la base de datos llamada 'sifme'

### Instalar
php artisan migrate

### Reinstalar Dev
php artisan migrate:refresh --seed

### Resintalar Prod
php artisan migrate:refresh

### Correr unicamente seeds
php artisan db:seed

