<?php

namespace SIFMEDE;
use SIFMEDE\Evento;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Auth;
//use Spatie\Permission\Traits\HasPermissions;

class User extends Authenticatable
{
    use HasRoles;
     
     public static function hasRightsOn($id){ // tiene derecho sobre lo que se le manda de ID
         return !Auth::guest() && (Auth::user()->hasPermissionTo('de_admin') || Auth::user()->id == $id);
     }
     
     public static function hasRights(){
         return !Auth::guest() && Auth::user()->hasPermissionTo('de_admin');
     }
     
    protected $fillable = [
        'first_name', 'email', 'password', 'last_name'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function noticias()
    {
        return $this->hasMany('SIFMEDE\Noticia');
    }

    public function following(){
        return $this->hasMany('SIFMEDE\SeguirNoticia','user_id');
    }

    public function assisting(){
        return $this->hasMany('SIFMEDE\Attending','user_id');
    }

     public function events()
    {
        return $this->hasMany('SIFMEDE\Evento');
    }
    public function eventos()
    {
        return $this->hasMany('SIFMEDE\Evento');
    }

    public function getId()
    {
      return $this->id;
    }
    
    public function comments(){
        return $this->hasMany('SIFMEDE\Comment','user_id');
    }

    public function attending(){
        return $this->hasMany('SIFMEDE\Attending','user_id');
    }
}
