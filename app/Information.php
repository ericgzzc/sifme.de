<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    //
    protected $table="information";
    public $timestamps=false;
    protected $fillable = [
    	'general_info',
    	'mission',
    	'vision',
    	'bank_name',
    	'bank_account',
    	'clabe',
    	'cardholders_name',
    	'company_name',
    	'contact_email',
    	'contact_phone',
    	'street',
    	'number',
    	'city',
    	'state',
    	'country'
    ];
}
