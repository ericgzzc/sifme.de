<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    protected $table = "comments";
     protected $dates = ['deleted_at'];
    
    public function noticia()
    {
        return $this->hasOne('SIFMEDE\RelNewComment', 'new_id');
    }
    
    public function evento(){
        return $this->hasOne('SIFMEDE\RelEventComment','event_id');
    }
    
    public function author(){
        return $this->belongsTo('SIFMEDE\User','user_id');
    }


}
