<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;

use SIFMEDE\Http\Requests;
use Input;
use Validator;
use Redirect;
use Session;
use Auth;


class FilesController extends Controller
{   
    
    public static $HOME = 1;
    public static $EVENTS = 2;
    public static $NEWS = 3;
    
    public function fileTest(){
        return view('test');
    }
    
    public function homeFile(Request $request){
        if(Auth::guest() ||!Auth::user()->hasPermissionTo('de_admin')){
            abort(401);
        }
        $file = Input::file('file');
        self::uploadFile(self::$HOME,$file);
    }
    
    public static function uploadFile($from,$file){
        if($file->isValid()){
            $extension = $file->getClientOriginalExtension();
            $basename = basename($file->getClientOriginalName(),'.'.$extension);
            $path = '';
            if($from == self::$EVENTS){
                $path = '/files/events/';
            }else if($from == self::$NEWS){
                $path = '/files/news/';
            }else if($from == self::$HOME){
                $path = '/images/home/';
            }else{
                return null;
            }
            $destinationPath = base_path() . '/public' . $path;
            $fileName = $basename . '-' . uniqid() . '.' . $extension; // renameing image
            $fullPath = $destinationPath . $fileName;
            $file->move($destinationPath, $fileName);
            
            return $path . $fileName;
        }else{
            return null;
        }
    }
    
    //TODO remover cosas
    public function uploadFileToHome(Request $request){
        
        $fileArr = array('file' => Input::file('file'));
        $rules = array('file' => 'required|mimes:jpg,jpeg,png,bmp,gif');
        
        $validator = Validator::make($fileArr, $rules);
        
        if ($validator->fails()) {
            Session::flash('flash_message', 'Solo se permiten imagenes con extension valida!');
            Session::flash('flash_type', 'alert-dismissible alert-warning');
            return redirect()->back();
        }
        
        $file = Input::file('file');
        
        $path = self::uploadFile(self::$HOME,$file);
        
        if(!is_null($path)){
            /*$destinationPath = base_path() . '/public/images/home/';
            $extension = $file->getClientOriginalExtension();
            $fileName = 'homeImage-' . uniqid() . '.' . $extension; // renameing image
            $file->move($destinationPath, $fileName);*/
            Session::flash('flash_message', 'Se subio la imagen con exito');
            Session::flash('flash_type', 'alert-dismissible alert-success');
        }else{
            Session::flash('flash_message', 'Tipo de archivo no soportado!');
            Session::flash('flash_type', 'alert-dismissible alert-warning');
        }
        return redirect()->back();
    }
    
    
    public function removeFileFromHome($url){
        
    }
    
    public function getFiles(){
        $dir = "images/home";
        $files = scandir($dir);
        $files = array_diff($files, array(".",".."));
        return response()->json(array_values($files));
    }
}
