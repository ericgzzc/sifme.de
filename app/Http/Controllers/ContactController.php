<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use SIFMEDE\Information;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use SIFMEDE\Http\Requests;

class ContactController extends Controller
{
  protected $request;

      public function __construct(\Illuminate\Http\Request $request) {
        $this->middleware('web');
        $this->request = $request;
    }

     public function index() {
      $contact = Information::all()->first();
      return View('contact.index', compact('contact')); 
    }

    public function update() {
      $input = $this->request->all();
      $contact = Information::all()->first();
      $contact->update($input);
      Session::flash('flash_message', '¡Información Guardada Correctamente!');
      Session::flash('flash_type', 'alert-dismissible alert-success');
      
      return Redirect::route('admin.contact.home');      
    }
}
