<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use SIFMEDE\Information;
use SIFMEDE\Http\Requests;
use Session;

class DonationsController extends Controller
{
    protected $request;

      public function __construct(\Illuminate\Http\Request $request) {
        $this->middleware('web');
        $this->request = $request;
    }

     public function index() {
      $donations = Information::all()->first();
      return View('donations.index', compact('donations')); 
  	}

  	public function update() {
      $input = $this->request->all();
      $donations = Information::all()->first();
      $donations->update($input);
      Session::flash('flash_message', '¡Información Guardada Correctamente!');
      Session::flash('flash_type', 'alert-dismissible alert-success');
      return Redirect::route('admin.donations.home');      
  	}
}
