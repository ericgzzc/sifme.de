<?php 

namespace SIFMEDE\Http\Controllers;
use SIFMEDE\RandomColor;
use SIFMEDE\Evento;
use SIFMEDE\ColorByMonth;
use SIFMEDE\User;
use SIFMEDE\EventsFile;
use SIFMEDE\Http\Controllers\ImageUpload;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use MaddHatter\LaravelFullcalendar\Event;
use SIFMEDE\Attending;
use Response;
use Illuminate\Support\Facades\Mail;

class EventosController extends Controller {


    protected $request;

      public function __construct(\Illuminate\Http\Request $request) {
        $this->middleware('web');
        $this->request = $request;
    }

    public function index() {
        $eventos = Evento::all();
        return view('events.index', compact('eventos'));
    }
    
    public function create()
    {
        return View('events.create');
    }

    public function show($id)
    {
        $evento = Evento::find($id);
        if (is_null($evento))
        {
            return Redirect::route('admin.events.home');
        }
         return view('events.show',compact('evento'));
    }

    public function edit($id)
    {
        $evento = Evento::find($id);
        if (is_null($evento))
        {
            return Redirect::route('admin.events.home');
        }

         return view('events.edit',compact('evento'));
        
    }
    public function update($id)
    {
    if(Auth::guest()){
      abort(401);
    }
    $evento = Evento::find($id);
    $input = $this->request->all();
    if(User::hasRightsOn($evento->author->id)){ // es propia o admin
      $evento->update($input);
      return response()->json(Evento::find($id));
    }else{
      abort(401);
    }
  }


  public function destroy($id) {
    if(Auth::guest()){
      abort(401);
    }
    $evento = Evento::find($id);
    if(User::hasRightsOn($evento->author->id)){ // es propia o admin
      Evento::destroy($id);
      return response()->json($evento); 
    }else{
      abort(401);
    }
  }


    public function store(Request $request){
        if(Auth::guest() || 
            (!Auth::user()->hasPermissionTo('de_admin')
                && !Auth::user()->hasPermissionTo('de_post_events'))){
                    abort(401);
                }
        $input = $request->all();
        $start_date =  Carbon::createFromFormat('d/m/Y H:i', $input['start_date'])->format('Y-m-d H:i');
        $end_date =  Carbon::createFromFormat('d/m/Y H:i', $input['end_date'])->format('Y-m-d H:i');
        $input['start_date'] =  $start_date;
        $input['end_date'] =  $end_date;
        $evento = new Evento($input);
        Auth::user()->events()->save($evento);
        return response()->json($evento);
    }
    /*
    public function store() {
        $input = $this->request->all();
        $userA = Auth::user();
        $start_date =  Carbon::createFromFormat('d/m/Y H:i', $this->request->start_date)->format('Y-m-d H:i');
        $end_date =  Carbon::createFromFormat('d/m/Y H:i', $this->request->end_date)->format('Y-m-d H:i');
        $input['start_date'] =  $start_date;
        $input['end_date'] =  $end_date;
        $userid = $userA->id;
        $user = User::find($userid);
        $event = new Evento($input);
        $event = $user->events()->save($event);
        if ($this->request->hasFile('imagen')) {
            $imagen = (new ImageUpload)->store($this->request, $event->id, $tipo = 3);
            $event->update(['imagen' => $imagen]);
            $url = ['url' => $imagen];
            $file = new EventsFile($url);
            $event->files()->save($file);
        }
        return Redirect::route('admin.events.home');
    }*/

    public function attends($id){
        
       return view('events.attends')->with('id', $id);
    }
  
    public function getAllfollowers($id){

        $evento = Evento::find($id);     
        $attending = Attending::where('event_id',$id)->get();
        //return view('events.attends',compact('users'));
        //
        $users = [];
        foreach ($attending as $user) {
            
            $user_id = $user->user->id;
            $usuario = User::find($user_id);
            $objeto = (object)[];
            $objeto->user_id = $user_id;
            $objeto->first_name = $usuario->first_name;
            $objeto->last_name = $usuario->last_name;
            $objeto->email = $usuario->email;
            array_push($users, $objeto);
        }

        return Response::json($users);


    }

    public function sendEmail($id, Request $request){

        $evento = Evento::find($id);     
        $followers = Attending::where('event_id',$id)->get();
        //return view('events.attends',compact('users'));
        $data = (object) $request->json()->all();
        $content= $data->mensaje;
        $subject = $data->subject;

        foreach ($followers as $user) {
            $user_id = $user->user->id;
            $usuario = User::find($user_id);

           
              Mail::raw($content, function($message) use($usuario, $subject)
              {
                  $message->from('no-reply@jesed.mx', 'Jesed');

                  $message->to($usuario->email)->subject($subject);
              });
            
        }

        return Response::json(array('success' => true));

    }

    

    public function rules() {
        return ['title' => 'required', 'image' => 'required|mimes:png'];
    }
    
    public function getEventsAdmin(){
        if(User::hasRights()){
            if(Auth::user()->hasPermissionTo('de_admin')){
                return response()->json(Evento::all());
            }else{
                return response()->json(Auth::user()->eventos());
            }
        }else{
            abort(401);
        }
    }
}

