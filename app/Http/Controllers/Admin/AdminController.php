<?php

namespace SIFMEDE\Http\Controllers\Admin;

use Illuminate\Http\Request;

use SIFMEDE\Http\Requests;
use SIFMEDE\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.welcome');
    }
    
   
}
