<?php 

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Redirect;
use SIFMEDE\User;
use SIFMEDE\SeguirNoticia;
use SIFMEDE\NewsFile;
use SIFMEDE\Http\Controllers\ImageUpload;
use SIFMEDE\Http\Controllers\FilesController;
use SIFMEDE\Http\Requests;
use Session;
use SIFMEDE\Noticia;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Response;
use Illuminate\Support\Facades\Mail;

class NewsController extends Controller {
  
  protected $request;

  public function __construct(\Illuminate\Http\Request $request) {
      $this->middleware('web');
      $this->request = $request;
  }
  
  public function getNewsAdmin(){
    if(Auth::guest()){
      return response()->json(array());// TODO regresar solo las noticias que sean publicas
    }else if(Auth::user()->hasPermissionTo('de_admin')){// Admin has all
      return response()->json(Noticia::all());
    }else{//
      return response()->json(Auth::user()->noticias());
    }
  } 
  
  // Vista con la lista de noticias
  public function index() {
      $noticias = Noticia::all();
      return View('news.index',compact('noticias')); 
  }

  // vista para crear una noticia
  public function create() {
    return View('news.create');
  }


  //servicio para crear la noticia
  /*
  public function store() {
    $input = $this->request->all();
    $userA = Auth::user();
    $userid = $userA->id;
    $user = User::find($userid);
    $noticia = new Noticia($input);
    $noticia = $user->noticias()->save($noticia);

    if ($this->request->hasFile('imagen')) {
        $imagen = (new ImageUpload)->store($this->request, $noticia->id, $tipo = 3);
        $noticia->update(['imagen' => $imagen]);
        $url = ['url' => $imagen];
        $file = new EventsFile($url);
        $noticia->files()->save($file);

    }
     return Redirect::route('noticias.show', $noticia->id);
  }*/
  
  // guardar noticias
  public function store(Request $request){
    if(User::hasRights()){
      $input = $this->request->all();
      $user = Auth::user();
      $noticia = $user->noticias()->save(new Noticia($input));
      return response()->json($noticia);
    }else{
      abort(401);
    }
  }
  
  
  public function show($id){
    $noticia = Noticia::find($id);
    return view('news.show',compact('noticia'));
  }

  //vista para editra
  public function edit($id) {

    $noticia = Noticia::find($id);
    return View('news.edit',compact('noticia'));
  }


  // Actualizar una noticia
  public function update($id){
    if(Auth::guest()){
      abort(401);
    }
    $noticia = Noticia::find($id);
    $input = $this->request->all();
    if(User::hasRightsOn($noticia->author->id)){ // es propia o admin
      $noticia->update($input);
      return response()->json(Noticia::find($id));
    }else{
      abort(401);
    }
  }

  // Eliminar noticia
  public function destroy($id){
    if(Auth::guest()){
      abort(401);
    }
    $noticia = Noticia::find($id);
    if(User::hasRightsOn($noticia->author->id)){ // es propia o admin
      Noticia::destroy($id);
      return response()->json($noticia); 
    }else{
      abort(401);
    }
  }
  
  public function getFile($id){
    $noticia = Noticia::find($id);
    if(User::hasRightsOn($noticia->author->id)){
      return response()->json($noticia->files);
    }else{
      abort(401);
    }
  }
  
  //fucion para agregar un archivo a la noticia
  public function addFile($id,Request  $input){
    $noticia = Noticia::find($id);
    if(User::hasRightsOn($noticia->author->id)){
      $file = Input::file('file');
      $path['url'] = FilesController::uploadFile(FilesController::$NEWS,$file);
      $response = $noticia->files()->save(new NewsFile($path));
      return response()->json($response);
    }else{
      abort(401);
    }
  }
  // funcion para eliminar un archivo a la noticia
  public function removeFile($id){
    $noticia = Noticia::find($id);
    if(User::hasRightsOn($noticia->author->id)){
      // do something
    }else{
      abort(401);
    }
  }

  public function followers($id){
       return view('news.followers')->with('id', $id);
    }

    public function getAllfollowers($id){
        $evento = Noticia::find($id);     
        $followers = SeguirNoticia::where('news_id',$id)->get();
        //return view('events.attends',compact('users'));
        //
        $users = [];
        foreach ($followers as $user) {
            $user_id = $user->user->id;
            $usuario = User::find($user_id);
            $objeto = (object)[];
            $objeto->user_id = $user_id;
            $objeto->first_name = $usuario->first_name;
            $objeto->last_name = $usuario->last_name;
            $objeto->email = $usuario->email;
            array_push($users, $objeto);
        }
        return Response::json($users);
       //return view('news.followers',compact('users'));
    }

    public function sendEmail($id, Request $request){

        $evento = Noticia::find($id);     
        $followers = SeguirNoticia::where('news_id',$id)->get();
        //return view('events.attends',compact('users'));
        $data = (object) $request->json()->all();
        $content= $data->mensaje;
        $subject = $data->subject;

        foreach ($followers as $user) {
            $user_id = $user->user->id;
            $usuario = User::find($user_id);

           
              Mail::raw($content, function($message) use($usuario, $subject)
              {
                  $message->from('no-reply@jesed.mx', 'Jesed');

                  $message->to($usuario->email)->subject($subject);
              });
            
        }

        return Response::json(array('success' => true));

    }


}