<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;

use SIFMEDE\Http\Requests;
use SIFMEDE\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use Carbon\Carbon;

class ImageUpload extends Controller
{
    public function store(Request $r, $id, $tipo)
    {
            $file = Input::file('imagen');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-' .$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name);
            return $name;
    }
}
