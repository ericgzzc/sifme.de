<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;

use SIFMEDE\Http\Requests;
use Session;
use SIFMEDE\User;
use SIFMEDE\UserPermissions;
use Auth;
use DB;

class UserController extends Controller
{
  // Vista con la lista de noticias
  public function index() {
      $users = User::all();
      return View('users.index',compact('users')); 
  }

  // vista para crear una noticia
  public function create() {
    $permisions = UserPermissions::getPermissionsTypes();
    return View('users.create',compact('permisions'));
  }

  //servicio para crear la noticia
  public function store(Request $request) {
    if(Auth::guest() || !Auth::user()->hasPermissionTo('de_admin')){
      abort(403,"Acceso restringido");//forbiden
    }
    
    // Obtenemos el objeto que se require crear
    $input = $request->all();
    // Creamos el usuario
    try {
      $user = User::create($input);
    }catch (\Illuminate\Database\QueryException $e){
      abort(400,"Correo duplicado");
    }
    //Si fue excitoso, creamos el row de permisos
    try{    
      foreach($input["permissions"] as $permission){
        if($permission["value"]){
          $user->givePermissionTo($permission["name"]);
        }
      }
    }catch (\Illuminate\Database\QueryException $e){
      $this->destroy($user->id);
      abort(500,"Error interno del servidor");
    }
    return response()->json($user);
  }
  
  public function show($id){
    $user = User::find($id);
    return View::make('user.show')->with('user',$user);
  }

  //vista para editra
  public function edit($id) {
    $user = User::find($id);
    
    if (is_null($user))
    {
      Session::flash('flash_message', 'No se encontró ese usuario!');
      Session::flash('flash_type', 'alert-dismissible alert-warning');
      return redirect()->action('UserController@index');
    }
    
    //return $permissions;
    return View('users.edit',compact('user'));
  }

  //actualiza una noticia
  public function update($id, Request $request) {
    if(Auth::guest() || !Auth::user()->hasPermissionTo('de_admin')){
      abort(403); // forbidden
    }
    
    $user = User::findOrFail($id);
    $input = $request->all();
    // Actualizamos el usuario
    $user->fill($input)->save();
    // Actualizamos permisos
    foreach($input["permissions"] as $permission){
      if($user->hasPermissionTo($permission["name"]) && !$permission["value"]){
        $user->revokePermissionTo($permission["name"]);
      }else if(!$user->hasPermissionTo($permission["name"]) && $permission["value"]){
        $user->givePermissionTo($permission["name"]);
      }
    }
    
    /*
    if(is_null($user->permissions()->get()[0])){
      // Si no existe creamos el nuevo set de permisos
      try{    
        $permissions = new UserPermissions($input);
        $permissions->user_id = $user->id;
        $permissions->save();
      }catch (PDOException $e){
        return $e;
      }
    }else{
      // Si existe set de permisos lo validamos y actualizamos
      $permissions = UserPermissions::findOrFail($user->permissions()->get()[0]->id);
      $permissions->fill($input)->save();
    }
    
    $user->fill($input)->save();
    
    Session::flash('flash_message','Usuario: '. $input['email'] .' actualizado exitosamente');
    */
    return response()->json($input);
  } 

  // Elimina una noticia
  
  //MODO REST
  public function destroy($id) {
    if(Auth::guest() || !Auth::user()->hasPermissionTo('de_admin')){
      abort(403,"Acceso restringido"); // forbidden
    }
    
    $user = User::findOrFail($id);
    User::destroy($id);
    return response()->json($user);
  } 
}
