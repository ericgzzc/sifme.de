<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;
use SIFMEDE\Comment;
use SIFMEDE\Noticia;
use SIFMEDE\Http\Requests;
use Auth;
use Response;
use SIFMEDE\User;
use SIFMEDE\RelNewComment;
use SIFMEDE\RelEventComment;
use SIFMEDE\Evento;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class CommentsController extends Controller
{

    function getNewsComments($id){
      $new = Noticia::find($id)->getCommentsInfo();
      return response()->json($new);
    }
    function getEventComments($id){
      $evento = Evento::find($id)->getCommentsInfo();
      return response()->json($evento);
    }

  	public function addCommentNoticia($id, Request $request)
    {
            $this->layout = null;
            //check if its our form
            $data = (object) $request->json()->all();
            $content= $data->comment;
          

            $userA = Auth::user();
            $userid = $userA->id;
      		  $user = User::find($userid);

              $comment = new Comment();
              $comment->comment = $content;
              $comment->user_id = $userid;
              $comment = $user->comments()->save($comment);
            

           $noticia = Noticia::find($id);
            $RelNewComment = new RelNewComment();
            $RelNewComment->new_id = $id;
            $RelNewComment->comment_id = $comment->id;
            $RelNewComment->save();

           return Response::json(array('success' => true, 'noticia_id' => $id));
      }
      public function addCommentEvento($id, Request $request)
    {
            $this->layout = null;
            //check if its our form
            $data = (object) $request->json()->all();
            $content= $data->comment;
          

            $userA = Auth::user();
            $userid = $userA->id;
            $user = User::find($userid);

              $comment = new Comment();
              $comment->comment = $content;
              $comment->user_id = $userid;
              $comment = $user->comments()->save($comment);
            

            $event = Evento::find($id);
            $RelEventComment = new RelEventComment();
            $RelEventComment->event_id = $id;
            $RelEventComment->comment_id = $comment->id;
            $RelEventComment->save();

           return Response::json(array('success' => true, 'evento_id' => $id));
      }

     public function deleteCommentNoticia($id){
            $userA = Auth::user();
            $userid = $userA->id;
            $user = User::find($userid);
            $comment = Comment::destroy($id);

            return Response::json(array('success' => true));
        
     }


     public function deleteNoticiaComment($id){
            $RelNewComment = RelNewComment::destroy($id);
            return Response::json(array('success' => true));
     }

      public function deleteCommentEvento($id){
            $userA = Auth::user();
            $userid = $userA->id;
            $user = User::find($userid);
            $comment = Comment::destroy($id);

            return Response::json(array('success' => true));
        
     }
     public function deleteEventoComment($id){
            $RelEventComment = RelEventComment::destroy($id);
            return Response::json(array('success' => true));
     }
}
