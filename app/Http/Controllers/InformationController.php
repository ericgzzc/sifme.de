<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;
use SIFMEDE\Http\Requests;
use Illuminate\Support\Facades\Input;
use SIFMEDE\Information;
use Illuminate\Support\Facades\Redirect;
use Session;

class InformationController extends Controller
{
    protected $request;

      public function __construct(\Illuminate\Http\Request $request) {
        $this->middleware('web');
        $this->request = $request;
    }

     public function index() {
      $info = Information::all()->first();
      return View('information.index', compact('info')); 
  	}

  	public function update() {
      $input = $this->request->all();
      $info = Information::all()->first();
      $info->update($input);

      Session::flash('flash_message', '¡Información Guardada Correctamente!');
      Session::flash('flash_type', 'alert-dismissible alert-success');
      return Redirect::route('admin.information.home');      
  	}
}
