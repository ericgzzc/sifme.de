<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use SIFMEDE\Http\Requests;
use SIFMEDE\User;
use Spatie\Permission\Models\Permission;

class ResourcesController extends Controller
{
    
    protected static function hasAdminAccess(){
        return !(Auth::guest() || !Auth::user()->hasPermissionTo('de_admin')); 
    }
    
    // Solo usuarios admin acceder a este recurso
    public function getAllUsers(){
        if(!self::hasAdminAccess()){
            return null;
        }
        $response = User::all();
        return response()->json($response);
    }
    
    //
    public function getUserPermissions($id){
        if(!self::hasAdminAccess()){
            return null;
        }
        $response = User::find($id)->permissions;
        return response()->json($response);
    }
    
    public function getPermissionNames(){
        if(!self::hasAdminAccess()){
            return null;
        }
        $response = Permission::all();
        return response()->json($response);
    }
}
