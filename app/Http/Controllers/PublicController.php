<?php

namespace SIFMEDE\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use SIFMEDE\Http\Requests;
use SIFMEDE\User;
use Session;
use Auth;
use SIFMEDE\Noticia;
use SIFMEDE\Attending;
use SIFMEDE\SeguirNoticia;
use DB;
use SIFMEDE\Evento;
use Carbon\Carbon;
use SIFMEDE\Information;
use Response;
use Illuminate\Support\Facades\Mail;


class PublicController extends Controller
{
    //
   public function __construct() {
      $this->middleware('web');
  }
  
  
  // Vista con la lista de noticias
  public function index() {
    $dir = "images/home";
    $files = scandir($dir);
    $files = array_diff($files, array(".",".."));
    return View('public.index',compact("files")); 
  }

  public function suscrito(){
    $news = Auth::user()->following;
    $noticias = [];
    foreach ($news as $new) {
      array_push($noticias, $new->noticia);
    }
        
    return View('public.noticiassigo', compact("noticias"));
  }

  public function asisto(){
    $events = Auth::user()->assisting;
    $eventos = [];
    foreach ($events as $evento) {
      array_push($eventos, $evento->evento);
    }
      return View('public.eventosasisto', compact('eventos'));
  }

  public function allnoticias() {
    $news = null;
    $noticias = [];
    if(Auth::guest()){
      $news = Noticia::where('public',1)->orderby('created_at', 'desc')->get();
      foreach ($news as $noticia) {
        $user= User::find($noticia->user_id);
        $noticia->author_first_name = $user->first_name;
        $noticia->author_last_name = $user->last_name;
        $noticia->seguir = false;
        $noticia->files;
        array_push($noticias, $noticia);
      }
    }else{
      $news = Noticia::orderBy('created_at', 'desc')->get();
      $userA = Auth::user();
      $userid = $userA->id;
      foreach ($news as $noticia) {
          $exists = SeguirNoticia::where('user_id',$userid)->where('news_id',$noticia->id)->get()->first();
          $user= User::find($noticia->user_id);
          $noticia->author_first_name = $user->first_name;
          $noticia->author_last_name = $user->last_name;
          $noticia->files;
          if(empty($exists)){
            $noticia->seguir = false;
            array_push($noticias, $noticia);
          }else{
            $noticia->seguir = true;
            array_push($noticias, $noticia);
          }
      }
    }
    return Response::json($noticias);
    
  }
  public function noticias() {
    
    return view('public.noticias');
  }
  public function usuarioContacto(){
      $userA = Auth::user();
      $userid = $userA->id;
      $user = User::find($userid);
        return Response::json($user);
  }
  public function leernoticia($id) {
    $noticia = Noticia::find($id);
    return view('public.leernoticia',compact('noticia'));
  }

  public function verevento($id) {
    $evento = Evento::find($id);
    return view('public.verevento',compact('evento'));
  }

  public function verinformacion() {
    $info = Information::all()->first();
    return view('public.information',compact('info'));
  }

  public function vermisionvision() {
    $info = Information::all()->first();
    return view('public.missionvision',compact('info'));
  }
  public function verdonaciones() {
    $info = Information::all()->first();
    return view('public.donations',compact('info'));
  }
  public function vercontacto() {
    $info = Information::all()->first();
    return view('public.contact',compact('info'));
  }

  public function calendario() {
    $event;
    $events = [];

    if(Auth::guest()){
      $event = Evento::where('public',1)->get();
    }else{
      $event = Evento::all();
    }
    

    foreach ($event as $eve) {
      $sdt = Carbon::parse($eve->start_date);
      $edt = Carbon::parse($eve->end_date);
      $events[] = \Calendar::event(
          $eve->title, //event title
          false, //full day event?
          $sdt->toW3cString(), //start time (you can also use Carbon instead of DateTime)
          $edt->toW3cString(), //end time (you can also use Carbon instead of DateTime)
          $eve->id, //optionally, you can specify an event ID
          [
              'url' => 'Evento/'.$eve->id,
              //any other full-calendar supported parameters
          ]
       );
    }

    $eloquentEvent = Evento::first(); //EventModel implements MaddHatter\LaravelFullcalendar\Event
    $calendar = \Calendar::addEvents($events) //add an array with addEvents
    ->setOptions([ //set fullcalendar options
        'firstDay' => 1,
        'aspectRatio'=> 2, 
    ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
        'eventClick' => 'function(event) {if (event.url) {
            location.href(event.url);
            return false;
        }}',
        'windowResize' =>  'function(view) {
        }',
    ]); 


    return View('public.calendar',compact('calendar'));
  }


  public function asistirEvento($id){

      $evento = Evento::find($id);
      $userA = Auth::user();
      $userid = $userA->id;
      $user = User::find($userid);
     
      $exists = Evento::find($id)->attends->where('user_id',$userid)->first();
      if(empty($exists)){
        $attend = new Attending();
        $attend->event_id= $evento->id;
        $attend->user_id = $userid;
        $attend->save();
        return Response::json(array('success' => true));
      }else{
        return Response::json(array('success' => false));
      }

      
  }
  public function noasistirEvento($id){

      $evento = Evento::find($id);
      $userA = Auth::user();
      $userid = $userA->id;
      $user = User::find($userid);
     
      $exists = Attending::where('user_id',$userid)->where('event_id',$id)->get()->first();
      
      return Response::json($exists->delete());
      
  }


  public function existsInAttending($id){
    $evento = Evento::find($id);
      $userA = Auth::user();
      $userid = $userA->id;
      $user = User::find($userid);
    $exists = Attending::where('user_id',$userid)->where('event_id',$id)->get()->first();
      if(empty($exists)){
        return Response::json(array('success' => true));
      }else{
        return Response::json(array('success' => false));
      }
  }


  public function seguirNoticia($id){
      $noticia = Noticia::find($id);
      $userA = Auth::user();
      $userid = $userA->id;
      $user = User::find($userid);
     
       $exists = SeguirNoticia::where('user_id',$userid)->where('news_id',$id)->get()->first();
      if(empty($exists)){
        $seguir = new SeguirNoticia();
        $seguir->news_id= $noticia->id;
        $seguir->user_id = $userid;
        $seguir->save();
        return Response::json(array('success' => true));
      }else{
        return Response::json(array('success' => false));
      }
  }

  public function unfollowNew($id){

      $evento = Noticia::find($id);
      $userA = Auth::user();
      $userid = $userA->id;
      $user = User::find($userid);
     
      $exists = SeguirNoticia::where('user_id',$userid)->where('news_id',$id)->get()->first();
      
      return Response::json($exists->delete());
      
  }

  public function existeEnNoticia($id){
      $evento = Noticia::find($id);
      $userA = Auth::user();
      $userid = $userA->id;
      $user = User::find($userid);
    $exists = SeguirNoticia::where('user_id',$userid)->where('news_id',$id)->get()->first();
      if(empty($exists)){
        return Response::json(array('success' => true));
      }else{
        return Response::json(array('success' => false));
      }
  }


  public function sendEmailContacto(Request $request){

      $data = (object) $request->json()->all();

      $dataCorreo = array(
        'subject' => $data->asunto,
        'name' => $data->name,
        'email' => $data->email,
        'content' => $data->mensaje,
      );


      Mail::send('emails.welcome', $dataCorreo ,  function($message) use($data)
              {
                  $message->from('no-reply@jesed.mx', 'Jesed');

                  $message->to('contact@jesed.mx')->subject($data->asunto);
      });

      return Response::json(array('success' => true));
      
      
  }


}
