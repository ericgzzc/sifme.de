<?php

namespace SIFMEDE\Http\Middleware;
use SIFMEDE\User;
use Closure;
use Auth;
class memberMW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guest()){
            return $next($request);
        }else{
            return redirect()->route('public.home');
        }
    }
}
