<?php

namespace SIFMEDE\Http\Middleware;

use Closure;
use Auth;
use SIFMEDE\User;

class adminMW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user() && Auth::user()->hasPermissionTo('de_admin')){
            return $next($request);
        }else {
            return redirect()->guest('Acceder');
        }
    }
}
