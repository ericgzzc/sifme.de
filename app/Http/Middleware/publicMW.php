<?php

namespace SIFMEDE\Http\Middleware;

use Closure;
use Auth;

class publicMW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guest()){
            return redirect()->route('member.home');
        }else{
            return $next($request);
        }
    }
}
