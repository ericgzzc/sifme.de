<?php

//RUTAS DE ADMIN
Route::group(['middleware' => ['web','admin']], function () {
  //root
  Route::get('/Admin/home',                     ['as'=> 'admin.home',               'uses' => 'HomeController@index']);
  Route::get('/Admin/',                         ['as'=> 'admin.home',               'uses' => 'HomeController@index']);
  
  //Miembros 
  Route::get(     'Admin/Miembros',             [ 'as' => 'admin.members.home',     'uses' => 'UserController@index']);
  Route::get(     'Admin/Miembros/Crear',       [ 'as' => 'admin.members.create',   'uses' => 'UserController@create']);
  Route::get(     'Admin/Miembros/{id}/Editar', [ 'as' => 'admin.members.edit',     'uses' => 'UserController@edit']);
  Route::get(     'Admin/Miembros/{id}',        [ 'as' => 'admin.members.show',     'uses' => 'UserController@show']);
  Route::delete(  'Admin/Miembros/{id}',        [ 'as' => 'admin.members.destroy',  'uses' => 'UserController@destroy']);
  Route::patch(   'Admin/Miembros/{id}',        [ 'as' => 'admin.members.update',   'uses' => 'UserController@update']);
  Route::post(    'Admin/Miembros/',            [ 'as' => 'admin.members.store',    'uses' => 'UserController@store']);
  
  // Eventos
  Route::get(     'Admin/Eventos',              [ 'as' => 'admin.events.home',      'uses' => 'EventosController@index']);
  Route::get(     'Admin/Eventos/Crear',        [ 'as' => 'admin.events.create',    'uses' => 'EventosController@create']);
  Route::get(     'Admin/Eventos/{id}/Editar',  [ 'as' => 'admin.events.edit',      'uses' => 'EventosController@edit']);
  Route::get(     'Admin/Eventos/{id}',         [ 'as' => 'admin.events.show',      'uses' => 'EventosController@show']);
  Route::delete(  'Admin/Eventos/{id}',         [ 'as' => 'admin.events.destroy',   'uses' => 'EventosController@destroy']);
  Route::patch(   'Admin/Eventos/{id}',         [ 'as' => 'admin.events.update',    'uses' => 'EventosController@update']);
  Route::post(    'Admin/Eventos',              [ 'as' => 'admin.events.store',     'uses' => 'EventosController@store']);
  Route::get(     'Admin/Eventos/Asistentes/{id}', [ 'as' => 'admin.events.attends',      'uses' => 'EventosController@attends']);
  Route::get(     'Admin/Eventos/Attending/{id}', [ 'as' => 'admin.events.getAllfollowers',      'uses' => 'EventosController@getAllfollowers']);
   Route::post(     'Admin/Eventos/enviarCorreo/{id}',             [ 'as' => 'admin.events.sendemail',        'uses' => 'EventosController@sendEmail']);

  
  // Noticias
  Route::get(     'Admin/Noticias',             [ 'as' => 'admin.news.home',        'uses' => 'NewsController@index']);
  Route::post(     'Admin/Noticias/enviarCorreo/{id}',             [ 'as' => 'admin.news.sendemail',        'uses' => 'NewsController@sendEmail']);
  Route::get(     'Admin/Noticias/Crear',       [ 'as' => 'admin.news.create',      'uses' => 'NewsController@create']);
  Route::get(     'Admin/Noticias/{id}/Editar', [ 'as' => 'admin.news.edit',        'uses' => 'NewsController@edit']);
  Route::get(     'Admin/Noticias/{id}',        [ 'as' => 'admin.news.show',        'uses' => 'NewsController@show']);
  Route::delete(  'Admin/Noticias/{id}',        [ 'as' => 'admin.news.destroy',     'uses' => 'NewsController@destroy']);
  Route::patch(   'Admin/Noticias/{id}',        [ 'as' => 'admin.news.update',      'uses' => 'NewsController@update']);
  Route::post(    'Admin/Noticias',             [ 'as' => 'admin.news.store',       'uses' => 'NewsController@store']);
  Route::get(     'Admin/Noticias/Seguidores/{id}', [ 'as' => 'admin.events.followers',      'uses' => 'NewsController@followers']);
  Route::get(     'Admin/Noticias/SeguidoresTodos/{id}', [ 'as' => 'admin.events.getAllfollowers',      'uses' => 'NewsController@getAllfollowers']);
  
  //FILES UPLOAD
  Route::post(    'Admin/Archivos/Home/',       [ 'as' => 'file.home.upload',       'uses' => 'FilesController@uploadFileToHome']);
  Route::delete(  'Admin/Archivos/Home/{url}',  [ 'as' => 'file.home.delete',       'uses' => 'FilesController@removeFileFromHome']);
  Route::get(     'Admin/Archivos/Home/',       [ 'as' => 'file.home.delete',       'uses' => 'FilesController@getFiles']);
  
  // General info
  Route::get(     'Admin/Informacion/',         [ 'as' => 'admin.information.home',   'uses' => 'InformationController@index']);
  Route::put(     'Admin/Informacion/',         [ 'as' => 'admin.information.update',   'uses' => 'InformationController@update']);
  Route::get(     'Admin/Donaciones/',          [ 'as' => 'admin.donations.home', 'uses' => 'DonationsController@index']);
  Route::put(     'Admin/Donaciones/',          [ 'as' => 'admin.donations.update', 'uses' => 'DonationsController@update']);
  Route::get(     'Admin/Contacto/',            [ 'as' => 'admin.contact.home',     'uses' => 'ContactController@index']);
  Route::put(     'Admin/Contacto/',            [ 'as' => 'admin.contact.update',     'uses' => 'ContactController@update']);
  
}); 

// RUTAS DE MIEMBRO
Route::group(['middleware' => ['web', 'member']], function(){
  // Noticias
  Route::get(   'Miembros/',                    [ 'as' => 'member.home',            'uses' => 'PublicController@index']);  
  ///
  Route::get(   'Miembros/Noticias/all',            [ 'as' => 'member.news.home',       'uses' => 'PublicController@allnoticias']);
  ///
  Route::get(   'Miembros/Noticias',            [ 'as' => 'member.news.home',       'uses' => 'PublicController@noticias']);
  Route::get(   'Miembros/Noticias/Suscrito',   [ 'as' => 'member.suscription.home', 'uses' => 'PublicController@suscrito']);
  Route::get(   'Miembros/Noticia/{id}',        [ 'as' => 'member.news.show',       'uses' => 'PublicController@leernoticia']);
  Route::post(  'Miembros/Noticia/{id}', [ 'as' => 'member.news.addComment', 'uses' => 'CommentsController@addCommentNoticia']);

  Route::delete(  'Miembros/Noticia/Comentario/{id}', [ 'as' => 'member.news.deleteComment', 'uses' => 'CommentsController@deleteCommentNoticia']);
  Route::delete(  'Miembros/Noticia/Comentario_Noticia/{id}', [ 'as' => 'member.news.deleteRelComment', 'uses' => 'CommentsController@deleteNoticiaComment']);
  Route::get(  'Miembros/Noticia/Comentarios/{id}', [ 'as' => 'member.news.getComments', 'uses' => 'CommentsController@getNewsComments']);
  Route::get(   'Miembros/Noticia/seguir/{id}',        [ 'as' => 'member.news.follow',       'uses' => 'PublicController@seguirNoticia']);
  Route::get('Miembros/Noticia/delete/{id}',        [ 'as' => 'member.news.unfollow',       'uses' => 'PublicController@unfollowNew']);
  Route::get(   'Miembros/Noticia/existe/{id}',        [ 'as' => 'member.news.follow',       'uses' => 'PublicController@existeEnNoticia']);


  // Eventos
  Route::get(   'Miembros/Evento/{id}',         [ 'as' => 'member.events.show',     'uses' => 'PublicController@verevento']);
  Route::get(   'Miembros/Calendario',          [ 'as' => 'member.events.calendar', 'uses' => 'PublicController@calendario']);
  Route::get(  'Miembros/Evento/asistir/{id}', [ 'as' => 'member.event.asistir', 'uses' => 'PublicController@asistirEvento']);
  Route::get(  'Miembros/Evento/no_asistir/{id}', [ 'as' => 'member.event.noasistir', 'uses' => 'PublicController@noasistirEvento']);
  Route::get(  'Miembros/Evento/Comentarios/{id}', [ 'as' => 'member.event.getComments', 'uses' => 'CommentsController@getEventComments']);
  Route::get(  'Miembros/Evento/existe/{id}', [ 'as' => 'member.event.existe', 'uses' => 'PublicController@existsInAttending']);
  Route::post(  'Miembros/Evento/{id}', [ 'as' => 'member.event.addComment', 'uses' => 'CommentsController@addCommentEvento']);
  Route::delete(  'Miembros/Evento/Comentario/{id}', [ 'as' => 'member.event.deleteComment', 'uses' => 'CommentsController@deleteCommentEvento']);
  Route::delete(  'Miembros/Evento/Comentario_Evento/{id}', [ 'as' => 'member.event.deleteRelComment', 'uses' => 'CommentsController@deleteEventoComment']);
  Route::get(   'Miembros/Calendario/Asisto',   [ 'as' => 'member.asists.home', 'uses' => 'PublicController@asisto']);

  // General info
  Route::get(   'Miembros/Informacion/',        [ 'as' => 'member.information.home',  'uses' => 'PublicController@verinformacion']);
  Route::get(   'Miembros/MisionVision/',       [ 'as' => 'member.missionvission.home','uses' => 'PublicController@vermisionvision']);
  Route::get(   'Miembros/Donaciones/',         [ 'as' => 'member.donation.home',   'uses' => 'PublicController@verdonaciones']);

  Route::get(   'Miembros/Contacto/',                     [ 'as' => 'member.contact.home',    'uses' => 'PublicController@vercontacto']);
  Route::get(   'Miembros/Contacto/usuario',              [ 'as' => 'member.contact.usuario',    'uses' => 'PublicController@usuarioContacto']);
  Route::post(  'Miembros/Contacto/sendEmail',            [ 'as' => 'member.contact.sendemail',    'uses' => 'PublicController@sendEmailContacto']);
  
  // LOGOUT
  Route::get('logout',                          ['as' => 'public.logout' ,        'uses' => 'Auth\AuthController@logout']);
});


// RUTAS PUBLICAS
Route::group(['middleware' => ['web','public']], function(){
  Route::get('/', ['as' => 'public.home', 'uses' => 'PublicController@index']);

  // Authentication Routes...
  Route::get('Acceder',                       ['as' => 'public.login'  ,        'uses' => 'Auth\AuthController@showLoginForm']);
  Route::post('Acceder',                      ['as' => 'public.login'  ,        'uses' => 'Auth\AuthController@preLogin']);

  Route::get('contrasena/reinicio/{token?}',  ['as' => 'public.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
  Route::post('contrasena/correo',            ['as' => 'public.passowrd.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
  Route::post('contrasena/reinicio',          ['as' => 'public.passowrd.reset', 'uses' => 'Auth\PasswordController@reset']);
  
  // Noticias
  Route::get(   'Usuarios/',                    [ 'as' => 'public.home',            'uses' => 'PublicController@index']);  
  Route::get(   'Usuarios/Noticias',            [ 'as' => 'public.news.home',       'uses' => 'PublicController@noticias']);
  Route::get(   'Usuarios/Noticia/{id}',        [ 'as' => 'public.news.show',       'uses' => 'PublicController@leernoticia']);
  Route::post(  'Usuarios/Noticia/comment/add', [ 'as' => 'public.news.addComment', 'uses' => 'CommentsController@addComment']);
  // Eventos
  Route::get(   'Usuarios/Evento/{id}',         [ 'as' => 'public.events.show',     'uses' => 'PublicController@verevento']);
  Route::get(   'Usuarios/Calendario',          [ 'as' => 'public.events.calendar', 'uses' => 'PublicController@calendario']);
  
  // General info
  Route::get(   'Usuarios/Informacion/',        [ 'as' => 'public.information.home',    'uses' => 'PublicController@verinformacion']);
  Route::get(   'Usuarios/MisionVision/',       [ 'as' => 'public.missionvission.home', 'uses' => 'PublicController@vermisionvision']);
  Route::get(   'Usuarios/Donaciones/',         [ 'as' => 'public.donation.home',       'uses' => 'PublicController@verdonaciones']);
  Route::get(   'Usuarios/Contacto/',           [ 'as' => 'public.contact.home',        'uses' => 'PublicController@vercontacto']);
  Route::get(   'Usuarios/Calendario/',         [ 'as' => 'public.calendar.home',       'uses' => 'PublicController@calendario']);

});

Route::group(['middleware' => ['web','resources']], function(){
  Route::get('Recursos/Usuarios',             ['as' => 'resources.get.allUsers',          'uses' => 'ResourcesController@getAllUsers']);
  Route::get('Recursos/Usuario/Permisos/{id}',['as' => 'resources.get.userPermissions',   'uses' => 'ResourcesController@getUserPermissions']);
  Route::get('Recursos/Permisos',             ['as' => 'resources.get.permissionNames',   'uses' => 'ResourcesController@getPermissionNames']);
  Route::get('Recursos/Noticias',             ['as' => 'resources.get.allNews',           'uses' => 'NewsController@getNewsAdmin']);
  Route::get('Recursos/Eventos',              ['as' => 'resources.get.allEvents',         'uses' => 'EventosController@getEventsAdmin']);
  
  //Modals
  Route::get('Modal/Noticia/Form/',           ['as' => 'modal.form.news',   'uses' => function() { return view('modals.newsForm'); } ] );
  Route::get('Modal/Miembro/Form/',           ['as' => 'modal.form.user',   'uses' => function() { return view('modals.userForm'); } ]);
  Route::get('Modal/Evento/Form/',           ['as' => 'modal.form.event',   'uses' => function() { return view('modals.eventsForm'); } ]);

  //news services
  Route::get(   'Recursos/Noticia/seguir/{id}',     [ 'as' => 'resources.get.news.follow',  'uses' => 'PublicController@seguirNoticia']);
  Route::get(   'Recursos/Noticias/all',            [ 'as' => 'resources.get.allNews',      'uses' => 'PublicController@allnoticias']);
  Route::patch( 'Recursos/Noticias/{id}',           [ 'as' => 'resources.patch.news',       'uses' => 'NewsController@update']);
  Route::post(  'Recursos/Noticias/',               [ 'as' => 'resources.post.news',        'uses' => 'NewsController@store']);
  Route::delete('Recursos/Noticias/{id}',           [ 'as' => 'resources.delete.news',      'uses' => 'NewsController@destroy']);
  Route::get(   'Recursos/Archivos/Noticia/{id}',   [ 'as' => 'resources.get.news.file',    'uses' => 'NewsController@getFile']);
  Route::post(  'Recursos/Archivos/Noticia/{id}',   [ 'as' => 'resources.post.news.file',   'uses' => 'NewsController@addFile']);
  Route::delete('Recursos/Archivos/Noticia/{id}',   [ 'as' => 'resources.delete.news.file', 'uses' => 'NewsController@deleteFile']);
  
  //events services
  
  Route::get(   'Recursos/Evento/seguir/{id}',      [ 'as' => 'resources.get.events.follow',  'uses' => 'EventsController@seguirNoticia']);
  Route::get(   'Recursos/Eventos/all',             [ 'as' => 'resources.get.allEvents',      'uses' => 'EventsController@allnoticias']);
  Route::patch( 'Recursos/Eventos/{id}',            [ 'as' => 'resources.patch.events',       'uses' => 'EventsController@update']);
  Route::post(  'Recursos/Eventos/',                [ 'as' => 'resources.post.events',        'uses' => 'EventsController@store']);
  Route::delete('Recursos/Eventos/{id}',            [ 'as' => 'resources.delete.events',      'uses' => 'EventsController@destroy']);
  Route::get(   'Recursos/Archivos/Evento/{id}',    [ 'as' => 'resources.get.events.file',    'uses' => 'EventsController@getFile']);
  Route::post(  'Recursos/Archivos/Evento/{id}',    [ 'as' => 'resources.post.events.file',   'uses' => 'EventsController@addFile']);
  Route::delete('Recursos/Archivos/Evento/{id}',    [ 'as' => 'resources.delete.events.file', 'uses' => 'EventsController@deleteFile']);
  
  // Generales
  Route::post(  'Recursos/Archivos/Inicio/',        [ 'as' => 'resources.post.home.files',        'uses' => 'FilesController@homeFile']);
});


Route::get(     'TEST',                       [ 'as' => 'file.index',       'uses' => 'FilesController@fileTest']);


Route::get('sendemail', function () {

    $data = array(
        'subject' => 'SPAAAAAAAAAMMMMM',
        'name' => 'Tania',
        'email' => 'hola@mail.com',
        'content' => 'SPAAAAAAAAAMMMMMSPAAAAAAAAAMMMMM
        SPAAAAAAAAAMMMMMSPAAAAAAAAAMMMMMSPAAAAAAAAAMMMMMSPAAAAAAAAAMMMMM
        SPAAAAAAAAAMMMMMSPAAAAAAAAAMMMMMSPAAAAAAAAAMMMMMSPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMMPAAAAAAAAAMMMMM',
      );

    Mail::send('emails.welcome', $data, function ($message) {

        $message->from('no-reply@jesed.mx', 'No-Reply');

        $message->to('jdfa14@gmail.com');

    });

    return "Your email has been sent successfully";

});

/*
// Lo de abajo DEBE SER reconsgtruiodp
 	//Route::get('/', 'HomeController@index');
 	Route::get('Noticias', 'NewsController@index');
	Route::get('Noticias/create', 'NewsController@create');
	
	Route::resource('comentarios','CommentsController');// NUNCA MAS USAR RESOURCE GADAMIT!

Route::get('images/{filename}', function ($filename)
{
    $path = storage_path() . '/' . $filename;

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
*/