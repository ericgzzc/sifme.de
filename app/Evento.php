<?php

namespace SIFMEDE;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evento extends Model implements \MaddHatter\LaravelFullcalendar\Event
{
    use SoftDeletes;
    protected $table = "events";
    protected $dates = ['deleted_at'];
   	protected $fillable=[
        'title',
        'content',
        'imagen',
        'address',
        'start_date',
        'end_date',
        'public'
    ];


      public function getCommentsInfo(){
        $array = [];
        foreach ($this->comments as $RelEventComment) {
            $comment = $RelEventComment->getComment;
            $user = $RelEventComment->getComment->author;

            $objeto = (object)[];
            $objeto->id = $comment->id;
            $objeto->id_rel = $RelEventComment->id;
            $objeto->user_id = $user->id;
            $objeto->first_name = $user->first_name;
            $objeto->last_name = $user->last_name;
            $objeto->created_at = $comment->created_at;
            $objeto->comment = $comment->comment;
            array_push($array, $objeto);
        }
        return $array;
    }

    public function author()
    {
        return $this->belongsTo('SIFMEDE\User','user_id');
    }

    public function files()
    {
        return $this->hasMany('SIFMEDE\EventsFile','event_id');
    }

    public function comments(){
        return $this->hasMany('SIFMEDE\RelEventComment','event_id');
    }

     public function attends(){
        return $this->hasMany('SIFMEDE\Attending','event_id');
    }
    
    public function getId() {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->all_day;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start_date;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end_date;
    }

    /**
     * Optional FullCalendar.io settings for this event
     *
     * @return array
     */
    public function getEventOptions()
    {
        return [
            'color' => $this->background_color,
            //etc
        ];
    }       

}
