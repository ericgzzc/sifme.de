<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeguirNoticia extends Model
{
    use SoftDeletes;
    protected $table = "seguir_noticia";
    protected $dates = ['deleted_at'];


     public function noticia()
    {
        return $this->belongsTo('SIFMEDE\Noticia','news_id');
    }
    
    public function user(){
        return $this->belongsTo('SIFMEDE\User','user_id');
    }
}
