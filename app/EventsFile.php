<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;
use SIFMEDE\Http\Controllers\FilesController; 

class EventsFile extends Model
{
    protected $table = 'events_files';
    public $timestamps = false; 

    protected $fillable=[
        'url', 'event_id'
    ];

    public function event()
    {
        return $this->belongsTo('SIFMEDE\Evento','event_id');
    }
    
    public static function create(array $attributes = []){
        
        if(is_nul($attributes['file'])){
            return null;
        }
        
        $url = FilesController::uploadFile(FilesController::$EVENTS,$attributes['file']);
        
        if(is_null($url)){
            return null;
        }
        
        $attributes['url'] = $url;
        parent::create($attributes);
    }
}
