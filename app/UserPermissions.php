<?php 

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserPermissions extends Model 
{
  static protected $tableName = "user_permissions";
  protected $table = "user_permissions";
  public $timestamps = false;
  
  // Lo que se puede rellenar al recibir los atributos
  protected $fillable = [
    'user_id', 'master', 'post_events', 'post_news', 'add_users', 'deactivate_users', 'delete_users'
  ];
  
  // Los campos que se obtienen pero no se imprimen
  protected $hidden = [
      
  ];

  static public function getPermissionsTypes() {
    $columns = DB::getSchemaBuilder()->getColumnListing(self::$tableName);
    $columns = array_values(array_diff($columns, array('id','user_id')));  
    return $columns;
  }
  
  //Relacion uno a uno
  public function user(){
    //User Id es el nombre del foreign que relaciona al Id del User
    return $this->belongsTo('SIFMEDE\User','user_id');
  }
}