<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;

class RelEventComment extends Model
{
    public $timestamps = false; 
    
    public function evento()
    {
        return $this->belongsTo('SIFMEDE\Evento','event_id');
    }
    
    public function getComment(){
        return $this->belongsTo('SIFMEDE\Comment','comment_id');
    }
}
