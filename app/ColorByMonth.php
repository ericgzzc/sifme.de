<?php 
namespace SIFMEDE;
use Carbon\Carbon;

class ColorByMonth
{

static private $colores = array(
			1 => "bar",
    		2 => "foo",
    		3 => "foo",
    		4 => "foo",
    		5 => "foo",
    		6 => "foo",
    		7 => "foo",
    		8 => "foo",
    		9 => "foo",
    		10 => "foo",
    		11 => "foo",
    		12 => "foo",
	);

 public function __construct() {

 	foreach (self::$colores as &$value) {
    	$value = RandomColor::one(); 
	}
 	
 }


	static public function getColor($date){
		$dt = Carbon::parse($date);
		return self::$colores[$dt->month];
	}

}