<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RelNewComment extends Model
{
	use SoftDeletes;
    public $timestamps = false; 
    protected $dates = ['deleted_at'];
    
    public function news(){
        return $this->belongsTo('SIFMEDE\Noticia','new_id');
    }
    
    public function getComment(){
        return $this->belongsTo('SIFMEDE\Comment','comment_id');
    }
}
