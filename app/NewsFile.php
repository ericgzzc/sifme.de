<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;

class NewsFile extends Model
{
    protected $table = 'news_files';
    public $timestamps = false;
    protected $fillable = [
    	'url'
    ];
    
    public function noticia(){
        return $this->belongsTo('SIFMEDE\Noticia', 'news_id');
    }
}