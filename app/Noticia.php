<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noticia extends Model
{
	use SoftDeletes;
    protected $table = "news";
    protected $dates = ['deleted_at'];
    protected $fillable = [
    	'title',
    	'content',
    	'imagen',
        'sector',
        'public'
    ];

    public function getCommentsInfo(){
        $array = [];
        foreach ($this->comments as $RelNewComment) {
            $comment = $RelNewComment->getComment;
            $user = $RelNewComment->getComment->author;

            $objeto = (object)[];
            $objeto->id = $comment->id;
            $objeto->id_rel = $RelNewComment->id;
            $objeto->user_id = $user->id;
            $objeto->first_name = $user->first_name;
            $objeto->last_name = $user->last_name;
            $objeto->created_at = $comment->created_at;
            $objeto->comment = $comment->comment;
            array_push($array, $objeto);
        }
        return $array;
    }

    public function comments()
    {
        return $this->hasMany('SIFMEDE\RelNewComment','new_id');
    }

    public function author()
    {
        return $this->belongsTo('SIFMEDE\User', 'user_id');
    }
    
    public function files(){
        return $this->hasMany('SIFMEDE\NewsFile','news_id');
    }
}


