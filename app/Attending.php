<?php

namespace SIFMEDE;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attending extends Model
{
    use SoftDeletes;
    protected $table = "attending";
    protected $dates = ['deleted_at'];


     public function evento()
    {
        return $this->belongsTo('SIFMEDE\Evento','event_id');
    }
    
    public function user(){
        return $this->belongsTo('SIFMEDE\User','user_id');
    }
}
